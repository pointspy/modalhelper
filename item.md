# item

```json

Instance of type DITableViewItem : 
{
  "rowSelectAttribute" : "kRT_CreateNewServiceAddressViewModelSelectRegionAttribute",
  "textTappedDelegate" : "<RT_CreateNewServiceViewController: 0x7fccc35c6310>",
  "loader" : false,
  "itemWhoResizeCell" : 0,
  "cellIdentifier" : "listCell",
  "tag" : 0,
  "viewCell" : "<DIMVVMTableViewCell: 0x7fccc40e7800; baseClass = UITableViewCell; frame = (0 55; 414 44); clipsToBounds = YES; autoresize = W; layer = <CALayer: 0x600000924c60>>",
  "cellHeight" : -1,
  "editingStyle" : 0,
  "cellStyle" : 0,
  "one" : "Пермский край",
  "containsClickableText" : false,
  "disabled" : false,
  "itemWhoResizeCellInset" : "UIEdgeInsets: {0, 0, 0, 0}",
  "accessoryType" : 1
}

Instance of type DITableViewItem : 
{
  "accessoryType" : 0,
  "one" : "<UIImage:0x600003a37600 named(main: icon_inet) {60, 60}>",
  "three" : false,
  "containsClickableText" : false,
  "loader" : false,
  "itemWhoResizeCellInset" : "UIEdgeInsets: {0, 0, 0, 0}",
  "cellIdentifier" : "serviceNameCell",
  "disabled" : false,
  "cellHeight" : 70,
  "cellStyle" : 0,
  "itemWhoResizeCell" : 0,
  "editingStyle" : 0,
  "tag" : 0,
  "two" : "Домашний Интернет",
  "viewCell" : "<DIMVVMTableViewCell: 0x7fccc501c000; baseClass = UITableViewCell; frame = (0 164; 414 70); clipsToBounds = YES; autoresize = W; layer = <CALayer: 0x6000008df060>>"
}

Instance of type DITableViewItem : 
{
  "accessoryType" : 0,
  "one" : "<UIImage:0x600003a37b10 named(main: icon_iptv) {60, 60}>",
  "three" : false,
  "containsClickableText" : false,
  "loader" : false,
  "itemWhoResizeCellInset" : "UIEdgeInsets: {0, 0, 0, 0}",
  "cellIdentifier" : "serviceNameCell",
  "disabled" : false,
  "cellHeight" : 70,
  "cellStyle" : 0,
  "itemWhoResizeCell" : 0,
  "editingStyle" : 0,
  "viewCell" : "<DIMVVMTableViewCell: 0x7fccc423f000; baseClass = UITableViewCell; frame = (0 234; 414 70); clipsToBounds = YES; autoresize = W; layer = <CALayer: 0x60000093c3e0>>",
  "tag" : 0,
  "two" : "Интерактивное ТВ"
}

Instance of type DITableViewItem : 
{
  "disabled" : false,
  "containsClickableText" : false,
  "editingStyle" : 0,
  "tag" : 0,
  "cellHeight" : 70,
  "itemWhoResizeCellInset" : "UIEdgeInsets: {0, 0, 0, 0}",
  "one" : "<UIImage:0x600003a19cb0 named(main: icon_pstn) {60, 60}>",
  "two" : "Домашний телефон",
  "cellIdentifier" : "serviceNameCell",
  "viewCell" : "<DIMVVMTableViewCell: 0x7fccc423da00; baseClass = UITableViewCell; frame = (0 304; 414 70); clipsToBounds = YES; autoresize = W; layer = <CALayer: 0x600000919020>>",
  "itemWhoResizeCell" : 0,
  "three" : false,
  "cellStyle" : 0,
  "loader" : false,
  "accessoryType" : 0
}

Instance of type DITableViewItem : 
{
  "disabled" : false,
  "containsClickableText" : false,
  "editingStyle" : 0,
  "tag" : 0,
  "cellHeight" : 70,
  "itemWhoResizeCellInset" : "UIEdgeInsets: {0, 0, 0, 0}",
  "one" : "<UIImage:0x600003a25a70 named(main: icon_gsm) {60, 60}>",
  "two" : "Мобильная связь",
  "cellIdentifier" : "serviceNameCell",
  "itemWhoResizeCell" : 0,
  "loader" : false,
  "three" : false,
  "cellStyle" : 0,
  "accessoryType" : 0,
  "viewCell" : "<DIMVVMTableViewCell: 0x7fccc5015200; baseClass = UITableViewCell; frame = (0 374; 414 70); clipsToBounds = YES; autoresize = W; layer = <CALayer: 0x60000092ae40>>"
}

Instance of type DITableViewItem : 
{
  "cellIdentifier" : "phoneCell",
  "textFieldAttribute" : "kRT_CreateNewServiceAddressViewModelPhoneAttribute",
  "loader" : false,
  "textTappedDelegate" : "<RT_CreateNewServiceViewController: 0x7fccc35c6310>",
  "editingStyle" : 0,
  "cellHeight" : -1,
  "itemWhoResizeCellInset" : "UIEdgeInsets: {0, 0, 0, 0}",
  "accessoryButtonTappedAttribute" : "kRT_CreateNewServiceAddressViewModelPhoneAttribute",
  "accessoryType" : 4,
  "viewCell" : "<DIMVVMTableViewCell: 0x7fccc4246000; baseClass = UITableViewCell; frame = (0 509; 414 44); clipsToBounds = YES; autoresize = W; tintColor = UIExtendedSRGBColorSpace 0.152941 0.227451 0.392157 1; layer = <CALayer: 0x60000091c6c0>>",
  "cellStyle" : 0,
  "tag" : 0,
  "itemWhoResizeCell" : 0,
  "disabled" : false,
  "containsClickableText" : false
}


```
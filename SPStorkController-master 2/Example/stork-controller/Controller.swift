import RxCocoa
import RxSwift
import UIKit

class Controller: UIViewController, HalfSheetPresentingProtocol {
    var transitionManager: HalfSheetPresentationManager!
    var viewModel: HomeViewModel!
    private let disposeBag = DisposeBag()

    var presentControllerButton = UIButton(type: UIButton.ButtonType.system)
    var presentTableControllerButton = UIButton(type: UIButton.ButtonType.system)

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            self.view.backgroundColor = UIColor.secondarySystemBackground
        } else {
            view.backgroundColor = UIColor.white
        }

        presentControllerButton.setTitle("Show ViewController", for: .normal)
        presentControllerButton.addTarget(self, action: #selector(presentModalViewController), for: .touchUpInside)
        presentControllerButton.sizeToFit()
        presentControllerButton.center.x = view.frame.width / 2
        presentControllerButton.center.y = view.frame.height / 4 * 3
        view.addSubview(presentControllerButton)
    }

    @objc func presentModalViewController() {
        
        
        
        let vm = BottomSheetLoginViewModelImpl(initialState: BottomSheetLoginState.initialState)
        
        let textVc = TextureModalLoginController(viewModel: vm)
        
//        self.present(textVc, animated: true, completion: nil)
        
        presentUsingHalfSheet(textVc)
        
       
    }

    // MARK: BindableType

    func bindViewModel() {
//        presentControllerButton.rx.tap
//            .bind(to: viewModel.input.presentModalTrigger)
//            .disposed(by: disposeBag)
    }

//    @objc func presentModalTableViewController() {
//        let transitionDelegate = Controller.transitionDelegate
//        transitionDelegate.storkDelegate = self
//        transitionDelegate.confirmDelegate = self.modalTable
//        self.modalTable.transitioningDelegate = transitionDelegate
//        self.modalTable.modalPresentationStyle = .custom
//        self.present(self.modalTable, animated: true, completion: nil)
//    }
}

extension Controller: HalfSheetCompletionProtocol {
    func didDismiss() {
        print("dismissed!")
    }
}


class TestVC: UIViewController, HalfSheetPresentableProtocol, HalfSheetTopVCProviderProtocol {

    var topVCTransitionStyle: HalfSheetTopVCTransitionStyle {
        return .slide
    }

    lazy var topVC: UIViewController = {
        return DismissBarVC.instance(tintColor: .white)
    }()

    var sheetHeight: CGFloat? = 250.0

    var managedScrollView: UIScrollView? {
        return nil
    }

    var dismissMethod: [DismissMethod] {
        return [.tap, .swipe]
    }
    
    var isLocked: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
    }

  
}

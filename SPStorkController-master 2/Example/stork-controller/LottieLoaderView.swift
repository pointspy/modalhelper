//
//  LottieLoaderView.swift
//  stork-controller
//
//  Created by Pavel Lyskov on 05.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//
// swiftlint:disable all
import Lottie
import UIKit

let loaderEdgeSize: CGFloat = 80.0


public final class LottieLoaderView: UIView {
    let loader = AnimationView()
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: loaderEdgeSize, height: loaderEdgeSize))
        
        setUpLoader()
    }
    
    private func setUpLoader() {
        
        if loader.animation == nil {
            if #available(iOS 13.0, *) {
            
            } else {
                // Fallback on earlier versions
            }
            loader.animation = Animation.filepath(aniPath(name: "loader3"))
            loader.animationSpeed = 1.5
        }
        
       
        /// A keypath that finds the color value for all `Fill 1` nodes.
        let fillKeypath = AnimationKeypath(keypath: "**.Stroke 1.Color")
        /// A Color Value provider that returns a reddish color.
        let newColor: UIColor
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        if #available(iOS 13.0, *) {
            newColor = UIColor.label
            newColor.getRed(&r, green: &g, blue: &b, alpha: &a)
        } else {
            newColor = UIColor.darkGray
            // Fallback on earlier versions
            
            newColor.getRed(&r, green: &g, blue: &b, alpha: &a)
        }
        
        let color = Color(r: Double(r), g: Double(g), b: Double(b), a: Double(a))
        
        let redValueProvider = ColorValueProvider(color)
        
        loader.backgroundColor = .clear
        
        /// Set the provider on the animationView.
        loader.setValueProvider(redValueProvider, keypath: fillKeypath)
        
        addSubview(loader)
        loader.loopMode = .loop
        

        
        loader.frame = self.bounds
        loader.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        loader.alpha = 0
        loader.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func start() {
        let timingParameters = UISpringTimingParameters(
            damping: 1.0,
            response: 0.15
        )
        
        let animator = UIViewPropertyAnimator(duration: 0, timingParameters: timingParameters)
        animator.addAnimations { [weak self] in
            guard let self = self else { return }
            self.loader.alpha = 1
            self.loader.transform = .identity
        }
        
        DispatchQueue.main.async {
            animator.startAnimation()
            self.loader.play()
        }
    }
    
    public func stop() {
        let timingParameters = UISpringTimingParameters(
            damping: 1.0,
            response: 0.15
        )
        
        let animator = UIViewPropertyAnimator(duration: 0, timingParameters: timingParameters)
        animator.addAnimations { [weak self] in
            guard let self = self else { return }
            self.loader.alpha = 0
            self.loader.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }
        
        DispatchQueue.main.async {
            animator.startAnimation()
            self.loader.stop()
        }
    }
    
    private func aniPath(name: String) -> String {
        let lottieBundlePath = Bundle.main.path(forResource: "lottie", ofType: "bundle") ?? ""
        let lottieBundle = Bundle(path: lottieBundlePath)
        let aniPath = lottieBundle?.path(forResource: name, ofType: "json") ?? ""

        return aniPath
    }
    
    public func setAnimation(with name: String) {
        loader.animation = Animation.filepath(aniPath(name: name))
    }
    
    public func successAnimation() {
        self.loader.animationSpeed = 1.0
        self.setAnimation(with: "send")
       
       
        
        DispatchQueue.main.async {
            
             self.start()
        }
        
       
    }
}

//
//  HomeViewModelImpl.swift
//  stork-controller
//
//  Created by Лысков Павел on 06.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import Action
import RxSwift
import XCoordinator

class HomeViewModelImpl: HomeViewModel, HomeViewModelInput, HomeViewModelOutput {

    // MARK: Inputs

    private(set) lazy var presentModalTrigger = presentAction.inputs

    // MARK: Actions

    private lazy var presentAction = CocoaAction { [weak self] in
        guard let self = self else {return .just({}())}
//        return self.router.rx.trigger(.modal)
//        presentUsingHalfSheet
        
        return .just({}())
    }

    // MARK: Stored properties

    private let router: UnownedRouter<AppRoute>

    // MARK: Initialization

    init(router: UnownedRouter<AppRoute>) {
        self.router = router
    }
}

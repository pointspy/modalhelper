import UIKit
import XCoordinator

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    private lazy var mainWindow = UIWindow()
    private let router = AppCoordinator().strongRouter

    func application(
        _: UIApplication,
        didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        let vc = Controller()
        
        mainWindow.rootViewController = vc
        mainWindow.makeKeyAndVisible()
        
        return true
    }

    func launch(rootViewController: UIViewController) {
        let frame = UIScreen.main.bounds
        window = UIWindow(frame: frame)
        window!.rootViewController = rootViewController
        window!.makeKeyAndVisible()
    }
}

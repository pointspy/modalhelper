//
//  ButtonsPanel.swift
//  stork-controller
//
//  Created by Лысков Павел on 03.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//
import RxCocoa
import RxSwift
import UIKit

protocol ButtonsPanelDelegate {
    func doneAction(sender: ButtonsPanel)
    func cancelAction(sender: ButtonsPanel)
}

final class ButtonsPanel: UIView {
    public var delegate: ButtonsPanelDelegate?
    public var doneHandler: (() -> Void)?
    public var cancelHandler: (() -> Void)?
    
    
    public private(set) var doneTitle: String = "Next" {
        didSet {
            guard oldValue != doneTitle else { return }

            self.doneButton.setTitle(doneTitle, for: .normal)
        }
    }

    public private(set) var cancelTitle: String = "Cancel" {
        didSet {
            guard oldValue != cancelTitle else { return }

            self.cancelButton.setTitle(cancelTitle, for: .normal)
        }
    }

    private var _doneButtonColor: UIColor = .systemBlue {
        didSet {
            doneButton.backgroundColor = _doneButtonColor
        }
    }

    public var doneButtonColor: UIColor {
        get { return _doneButtonColor }
        set(newValue) {
            _doneButtonColor = newValue
        }
    }

    private var _buttonCornerRadius: CGFloat = 10.0 {
        didSet {
            self.doneButton.layer.cornerRadius = _buttonCornerRadius
        }
    }

    public var buttonCornerRadius: CGFloat {
        get { return _buttonCornerRadius }
        set {
            _buttonCornerRadius = newValue
        }
    }

    public let stackView = UIStackView()
    
    public var doneButtonShared: UIButton {
        return doneButton
    }
    
    public var cancelButtonShared: UIButton {
        return cancelButton
    }

    private lazy var doneButton: UIButton = {
        let button = UIButton()
        button.setTitle(self.doneTitle, for: .normal)
        
        button.setTitleColor(.white, for: .normal)
        button.layer.masksToBounds = true
        button.backgroundColor = _doneButtonColor
        button.layer.cornerRadius = _buttonCornerRadius
        
        if let label = button.titleLabel {
            label.font = .systemFont(ofSize: 16, weight: .semibold)
        }
        
        return button
    }()

    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle(self.cancelTitle, for: .normal)
        if #available(iOS 13.0, *) {
            button.setTitleColor(.label, for: .normal)
        } else {
            button.setTitleColor(.black, for: .normal)
        }
        button.layer.masksToBounds = true
        if #available(iOS 13.0, *) {
            button.layer.borderColor = UIColor.quaternaryLabel.cgColor
        } else {
            button.layer.borderColor = UIColor.black.withAlphaComponent(0.2).cgColor
        }
        button.layer.borderWidth = 1.0
        button.layer.cornerRadius = _buttonCornerRadius
        
        if let label = button.titleLabel {
            label.font = .systemFont(ofSize: 16, weight: .semibold)
        }
        
        return button
    }()

    required init() {
       
        super.init(frame: .zero)

        setUpStackView()
        setUpButtons()
        setUpConstraints()
    }

    private func setUpStackView() {
        stackView.axis = .horizontal
        stackView.spacing = 16.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
    }

    private func setUpButtons() {
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        doneButton.translatesAutoresizingMaskIntoConstraints = false
        doneButton.addTarget(self, action: #selector(doneAction), for: .touchUpInside)

        stackView.addArrangedSubview(cancelButton)
        stackView.addArrangedSubview(doneButton)
    }

    @objc private func cancelAction() {
        if let delegate = self.delegate {
            delegate.cancelAction(sender: self)
        }

        if let handler = cancelHandler {
            handler()
        }
    }

    @objc private func doneAction() {
        if let delegate = self.delegate {
            delegate.doneAction(sender: self)
        }

        if let handler = doneHandler {
            handler()
        }
    }

    private func setUpConstraints() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),

        ])

        NSLayoutConstraint.activate([
            cancelButton.heightAnchor.constraint(equalToConstant: 52),
            cancelButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.35),
            doneButton.heightAnchor.constraint(equalToConstant: 52),
            doneButton.widthAnchor.constraint(greaterThanOrEqualTo: widthAnchor, multiplier: 0.6)
        ])
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func setDone(title: String) {
        doneTitle = title
    }

    public func setCancel(title: String) {
        cancelTitle = title
    }

    public func setTitles(done: String, cancel: String) {
        setDone(title: done)
        setCancel(title: cancel)
    }
    
    public func set(enabled: Bool) {
        self.doneButton.isEnabled = enabled
        self.cancelButton.isEnabled = enabled
        
        let timingParameters = UISpringTimingParameters(
            damping: 1.0,
            response: 0.217
        )

        let animator = UIViewPropertyAnimator(duration: 0, timingParameters: timingParameters)
        animator.addAnimations {
            self.doneButton.alpha = enabled ? 1.0 : 0.6
            self.cancelButton.alpha = enabled ? 1.0 : 0.6
        }
        animator.startAnimation()
        
        
    }
    
    public func hideCancel(withDone doneTitle: String) {
        
        let timingParameters = UISpringTimingParameters(
            damping: 1.0,
            response: 0.217
        )

        let animator = UIViewPropertyAnimator(duration: 0, timingParameters: timingParameters)
        animator.addAnimations {
            self.cancelButton.alpha = 0
            self.stackView.arrangedSubviews[0].isHidden = true
        }
        animator.startAnimation()
        self.setDone(title: doneTitle)
    }
    
    public func showCancel() {
        
        let timingParameters = UISpringTimingParameters(
            damping: 1.0,
            response: 0.217
        )

        let animator = UIViewPropertyAnimator(duration: 0, timingParameters: timingParameters)
        animator.addAnimations {
            self.cancelButton.alpha = 1
            self.stackView.arrangedSubviews[0].isHidden = false
        }
        animator.startAnimation()
        
    }
}

//
//  AppCoordinator.swift
//  stork-controller
//
//  Created by Лысков Павел on 06.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import UIKit
import XCoordinator

enum AppRoute: Route {
    case home
    case modal
}

class AppCoordinator: NavigationCoordinator<AppRoute> {
    // MARK: Initialization

    init() {
        super.init(initialRoute: .home)
    }

    // MARK: Overrides

    override func prepareTransition(for route: AppRoute) -> NavigationTransition {
        switch route {
        case .home:
            let viewController = Controller()
            let viewModel = HomeViewModelImpl(router: unownedRouter)
//            viewController.bind(to: viewModel)
            return .push(viewController)

        case .modal:
            let controller = UIViewController()

//            let initialState = BottomSheetState.initialState
//
//            let height = initialState.getCurrentHeight()
//
//            let viewModel = BottomSheetViewModelImpl(initialState: initialState)
//            controller.bind(to: viewModel)
//
//       
//            controller.currentHeight = height
//            controller.modalPresentationStyle = .custom

            return .present(controller)
        }
    }
}

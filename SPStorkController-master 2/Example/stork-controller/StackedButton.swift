//
//  StackedButton.swift
//  stork-controller
//
//  Created by Лысков Павел on 28.02.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import UIKit

final public class StackedButton: UIButton {
    
    override public var intrinsicContentSize: CGSize {
        return CGSize(width: 120, height: 68)
    }
    
}

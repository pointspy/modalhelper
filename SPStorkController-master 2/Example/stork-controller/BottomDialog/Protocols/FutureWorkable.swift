//
//  FutureWorkable.swift
//  stork-controller
//
//  Created by Pavel Lyskov on 18.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import AsyncDisplayKit

public protocol FutureWorkable: AnyObject {
    var _viewAssociations: [(UIView) -> Void]? { get set }
}

public extension FutureWorkable where Self: ASDisplayNode {
    func appendViewAssociation(block: @escaping (UIView) -> Void) {
        if self.isNodeLoaded {
            block(view)
        } else {
            if _viewAssociations == nil {
                _viewAssociations = [(UIView) -> Void]()
            }
            _viewAssociations!.append(block)
        }
    }

    func viewAssociationWhenNodeLoaded() {
        guard let viewAssociations = _viewAssociations else {
            return
        }
        for block in viewAssociations {
            block(view)
        }
        _viewAssociations = nil
    }
}

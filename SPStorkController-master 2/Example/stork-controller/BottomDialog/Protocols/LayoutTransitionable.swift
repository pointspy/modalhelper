//
//  LayoutTransitionable.swift
//  stork-controller
//
//  Created by Лысков Павел on 18.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import AsyncDisplayKit

protocol LayoutTransitionable {
    var presentedNodes: [ASDisplayNode] { get }
    var removedNodes: [ASDisplayNode] { get }
    var retriverNodes: [ASDisplayNode] { get }
}

//
//  ViewTransitionable.swift
//  stork-controller
//
//  Created by Лысков Павел on 28.02.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import UIKit

public protocol ViewTransitionable: AnyObject {
    var viewsForTransitionAppear: [UIView] { get }
    var apearDurationForView: TimeInterval { get }
    var recursiveDepth: Int { get }
    var containerForViews: UIView? { get }
    var interStepDuration: TimeInterval? { get }
    var commonDelay: TimeInterval? { get }
    var apearDurationForContainer: TimeInterval? { get }
    var customDuration: TimeInterval {get}
}


extension HalfSheetPresentableProtocol where Self: UIViewController {
    
    func getOptimizedDuration(minDuration: TimeInterval, maxDuration: TimeInterval, minTranslation: CGFloat, maxTranslation: CGFloat) -> TimeInterval {
        
        let linear = Easing.linear
        
        return linear.calculateOptimizedTime(for: self.sheetHeight ?? 500.0, minDuration: minDuration, maxDuration: maxDuration, minTranslation: minTranslation, maxTranslation: maxTranslation)
        
    }
    
}




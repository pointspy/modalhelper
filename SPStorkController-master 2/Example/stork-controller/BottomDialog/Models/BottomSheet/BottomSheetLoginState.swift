//
//  BottomSheetState.swift
//  stork-controller
//
//  Created by Лысков Павел on 06.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import UIKit
// swiftlint:disable all

struct BottomSheetLoginState: Equatable {
    struct Color {
        static var title: UIColor = {
            if #available(iOS 13.0, *) {
                return .label
            } else {
                return .black
            }
        }()

        static var message: UIColor = {
            if #available(iOS 13.0, *) {
                return .secondaryLabel
            } else {
                return .gray
            }
        }()

        static var background: UIColor = {
            if #available(iOS 13.0, *) {
                return .systemBackground
            } else {
                return .white
            }
        }()

        static var buttonBorder: UIColor = {
            if #available(iOS 13.0, *) {
                return .tertiaryLabel
            } else {
                return UIColor(white: 0.2, alpha: 1)
            }
        }()

        static var buttonTitle: UIColor = {
            UIColor.white

        }()

        static let placeHolderColor: UIColor = UIColor.gray.withAlphaComponent(0.2)
    }

    static func == (lhs: BottomSheetLoginState, rhs: BottomSheetLoginState) -> Bool {
        let result = lhs.currentVisibles == rhs.currentVisibles &&
            lhs.doneButtonTitle == rhs.doneButtonTitle &&
            lhs.errorMessage == rhs.errorMessage &&
            lhs.height == rhs.height &&
            lhs.indicatorHeight == rhs.indicatorHeight &&
            lhs.isError == rhs.isError &&
            lhs.lifeCycleState == rhs.lifeCycleState &&
            lhs.message == rhs.message &&
            lhs.paddings == rhs.paddings &&
            lhs.successMessage == rhs.successMessage &&
            lhs.title == rhs.title

        return result
    }

    typealias ActionBlock = () -> Void

    static var titleFont: UIFont = UIFont.boldSystemFont(ofSize: 24)
    static var messageFont: UIFont = UIFont.systemFont(ofSize: 17)

    static var initialState: BottomSheetLoginState = {
        let temp = "Для продолжения работы необходима авторизация.".at.attributed {
            $0.font(messageFont).foreground(color: Color.message)
        }

        let visibles = BottomSheetLoginState
            .Visible(title: true, message: true, indicator: false, buttons: true, textFields: true)

        return BottomSheetLoginState(
            title: "Acceptio",
            message: temp,
            doneTitle: "Продолжить",
            paddings: .init(
                leftRight: 24,
                titleTop: 24,
                titleBottom: 24,
                messageTop: 16,
                messageBottom: 16,
                buttonsTop: 36,
                buttonsBottom: 24,
                loginTop: 16,
                passwordTop: 16,
                textFieldHeight: 44,
                indicatorTop: 16,
                indicatorBottom: 24

            ),
            lifeCycleState: .initial,
            successMessage: "Авторизация успешно завершена.".at.attributed {
                $0.font(messageFont).foreground(color: Color.message)
            },
            errorMessage: "Ошибка авторизации. Проверьте введенные данные и повторите попытку.".at
                .attributed {
                    $0.font(messageFont).foreground(color: Color.message)
                },
            indicatorHeight: 80,
            currentVisibles: visibles,
            nextAction: {}
        )
    }()

    struct Padding: Equatable {
        var leftRight: CGFloat
        var titleTop: CGFloat
        var titleBottom: CGFloat
        var messageTop: CGFloat
        var messageBottom: CGFloat
        var buttonsTop: CGFloat
        var buttonsBottom: CGFloat
        var loginTop: CGFloat
        var passwordTop: CGFloat
        var textFieldHeight: CGFloat
        var indicatorTop: CGFloat
        var indicatorBottom: CGFloat
    }

    struct Visible: Equatable {
        var title: Bool
        var message: Bool
        var indicator: Bool
        var buttons: Bool
        var textFields: Bool
    }

    enum LifeCycleState {
        case initial
        case waitingForResponse
        case success
    }

    var title: String
    var message: NSAttributedString
    var doneButtonTitle: String
    var paddings: Padding
    var lifeCycleState: LifeCycleState
    var height: CGFloat
    var successMessage: NSAttributedString
    var errorMessage: NSAttributedString
    var indicatorHeight: CGFloat
    var currentVisibles: Visible
    var isError: Bool = false
    var nextAction: ActionBlock

    init(
        title: String,
        message: NSAttributedString,
        doneTitle: String,
        paddings: BottomSheetLoginState.Padding,
        lifeCycleState: LifeCycleState,
        successMessage: NSAttributedString,
        errorMessage: NSAttributedString,
        indicatorHeight: CGFloat,
        currentVisibles: Visible,
        nextAction: @escaping ActionBlock
    ) {
        self.title = title
        self.message = message
        doneButtonTitle = doneTitle
        self.paddings = paddings

        self.lifeCycleState = lifeCycleState
        self.indicatorHeight = indicatorHeight
        self.errorMessage = errorMessage
        self.successMessage = successMessage
        self.currentVisibles = currentVisibles
        let titleHeight = currentVisibles.title ? BottomSheetLoginState.titleFont.heightFor(
            text: title,
            width: WIDTH - paddings.leftRight * 2
        ) + paddings.titleTop + paddings.titleBottom : 0.0
        let buttonsHeight: CGFloat = currentVisibles.buttons ? 52 + 4 + paddings.buttonsTop + paddings
            .buttonsBottom : 0.0
        let messageHeight = currentVisibles.message ? paddings.messageBottom + paddings.messageTop + message.height(for: WIDTH - 2 * paddings.leftRight) : 0.0
        let indHeight = currentVisibles.indicator ? indicatorHeight + paddings.indicatorTop + paddings.indicatorBottom : 0.0

        let textFieldsHeight = currentVisibles.textFields ? paddings.textFieldHeight * 2 + paddings.loginTop + paddings.passwordTop : 0.0

        height = titleHeight + buttonsHeight + messageHeight + indHeight + textFieldsHeight

        self.nextAction = nextAction
    }

    mutating func updateHeight() {
        height = getCurrentHeight()
    }

    mutating func setError(with value: Bool) {
        isError = value
    }

    func getCurrentHeight() -> CGFloat {
        let titleHeight = currentVisibles.title ? BottomSheetLoginState.titleFont.heightFor(
            text: title,
            width: WIDTH - paddings.leftRight * 2
        ) + paddings.titleTop + paddings.titleBottom : 0.0
        let buttonsHeight: CGFloat = currentVisibles.buttons ? 52 + 4 + paddings.buttonsTop + paddings
            .buttonsBottom : 0.0
        let messageHeight = currentVisibles.message ? paddings.messageBottom + paddings.messageTop + message.height(for: WIDTH - paddings.leftRight * 2) : 0.0
        let indHeight = currentVisibles.indicator ? indicatorHeight + paddings.indicatorTop + paddings.indicatorBottom : 0.0

        let textFieldsHeight = currentVisibles.textFields ? paddings.textFieldHeight * 2 + paddings.loginTop + paddings.passwordTop : 0.0

        return titleHeight + messageHeight + buttonsHeight + indHeight + textFieldsHeight
    }

    mutating func set(title: String) {
        self.title = title
    }

    mutating func set(message: NSAttributedString) {
        self.message = message
    }

    mutating func set(visibles: Visible) {
        currentVisibles = visibles
    }

    public func new(with lifeCycleState: LifeCycleState, visibles: Visible) -> BottomSheetLoginState {
        return BottomSheetLoginState(
            title: title,
            message: message,
            doneTitle: doneButtonTitle,
            paddings: paddings,
            lifeCycleState: lifeCycleState,
            successMessage: successMessage,
            errorMessage: errorMessage,
            indicatorHeight: indicatorHeight,
            currentVisibles: visibles,
            nextAction: nextAction
        )
    }

    public func newFor(success: Bool, with visibles: Visible, loaderHeight: CGFloat = 100, newMessage: NSAttributedString) -> BottomSheetLoginState {
        var newState = BottomSheetLoginState(
            title: title,
            message: newMessage,
            doneTitle: doneButtonTitle,
            paddings: paddings,
            lifeCycleState: success ? LifeCycleState.success : LifeCycleState.initial,
            successMessage: success ? newMessage : successMessage,
            errorMessage: !success ? newMessage : errorMessage,
            indicatorHeight: loaderHeight,
            currentVisibles: visibles,
            nextAction: nextAction
        )

        newState.setError(with: success)

        return newState
    }

    public func fromInitial(with title: String, message: NSAttributedString, successMessage: NSAttributedString, nextAction: @escaping ActionBlock) -> BottomSheetLoginState {
        return BottomSheetLoginState(title: title, message: message, doneTitle: BottomSheetLoginState.initialState.doneButtonTitle, paddings: BottomSheetLoginState.initialState.paddings, lifeCycleState: BottomSheetLoginState.initialState.lifeCycleState, successMessage: successMessage, errorMessage: BottomSheetLoginState.initialState.errorMessage, indicatorHeight: BottomSheetLoginState.initialState.indicatorHeight, currentVisibles: BottomSheetLoginState.initialState.currentVisibles, nextAction: nextAction)
    }
}

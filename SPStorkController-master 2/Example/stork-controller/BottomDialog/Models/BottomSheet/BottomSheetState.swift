//
//  BottomSheetState.swift
//  stork-controller
//
//  Created by Лысков Павел on 06.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import UIKit
// swiftlint:disable all
let WIDTH = UIScreen.main.bounds.width

struct BottomSheetState: Equatable {
    static func == (lhs: BottomSheetState, rhs: BottomSheetState) -> Bool {
        let result = lhs.cancelButtonTitle == rhs.cancelButtonTitle &&
            lhs.currentVisibles == rhs.currentVisibles &&
            lhs.doneButtonTitle == rhs.doneButtonTitle &&
            lhs.errorMessage == rhs.errorMessage &&
            lhs.height == rhs.height &&
            lhs.indicatorHeight == rhs.indicatorHeight &&
            lhs.isError == rhs.isError &&
            lhs.lifeCycleState == rhs.lifeCycleState &&
            lhs.message == rhs.message &&
            lhs.paddings == rhs.paddings &&
            lhs.successMessage == rhs.successMessage &&
            lhs.title == rhs.title

        return result
    }

    typealias ActionBlock = () -> Void

    static var titleFont: UIFont = UIFont.boldSystemFont(ofSize: 24)
    static var messageFont: UIFont = UIFont.systemFont(ofSize: 17)

    static var initialState: BottomSheetState = {
        let temp = """
        A simple class for laying out a collection of views with a convenient API, while leveraging the power of Auto Layout.
        A simple class for laying out a collection of views with a convenient API, while leveraging the power of Auto Layout.
        A simple class for laying out a collection of views with a convenient API, while leveraging the power of Auto Layout.
        """

        let visibles = BottomSheetState
            .Visible(title: true, message: true, indicator: false, buttons: true, cancelButton: true)

        return BottomSheetState(
            title: "Welcome!",
            message: temp,
            doneTitle: "Продолжить",
            cancelButtonTitle: "Отмена",
            paddings: .init(
                leftRight: 24,
                titleTop: 60,
                titleBottom: 24,
                messageTop: 36,
                messageBottom: 24,
                buttonsTop: 80,
                buttonsBottom: 24
            ),
            lifeCycleState: .initial,
            successMessage: "Success!! very good",
            errorMessage: "Some error occurs",
            indicatorHeight: 80,
            currentVisibles: visibles,
            nextAction: {}
        )
    }()

    struct Padding: Equatable {
        var leftRight: CGFloat
        var titleTop: CGFloat
        var titleBottom: CGFloat
        var messageTop: CGFloat
        var messageBottom: CGFloat
        var buttonsTop: CGFloat
        var buttonsBottom: CGFloat
    }

    struct Visible: Equatable {
        var title: Bool
        var message: Bool
        var indicator: Bool
        var buttons: Bool
        var cancelButton: Bool
    }

    enum LifeCycleState {
        case initial
        case waitingForResponse
        case success
        case error
    }

    var title: String
    var message: String
    var doneButtonTitle: String
    var cancelButtonTitle: String
    var paddings: Padding
    var lifeCycleState: LifeCycleState
    var height: CGFloat
    var successMessage: String
    var errorMessage: String
    var indicatorHeight: CGFloat
    var currentVisibles: Visible
    var isError: Bool = true
    var nextAction: ActionBlock

    init(
        title: String,
        message: String,
        doneTitle: String,
        cancelButtonTitle: String,
        paddings: Padding,
        lifeCycleState: LifeCycleState,
        successMessage: String,
        errorMessage: String,
        indicatorHeight: CGFloat,
        currentVisibles: Visible,
        nextAction: @escaping ActionBlock
    ) {
        self.title = title
        self.message = message
        doneButtonTitle = doneTitle
        self.paddings = paddings
        self.cancelButtonTitle = cancelButtonTitle
        self.lifeCycleState = lifeCycleState
        self.indicatorHeight = indicatorHeight
        self.errorMessage = errorMessage
        self.successMessage = successMessage
        self.currentVisibles = currentVisibles
        let titleHeight = currentVisibles.title ? BottomSheetState.titleFont.heightFor(
            text: title,
            width: WIDTH - paddings.leftRight * 2
        ) + paddings.titleTop + paddings.titleBottom : 0.0
        let buttonsHeight: CGFloat = currentVisibles.buttons ? 52 + 4 + paddings.buttonsTop + paddings
            .buttonsBottom : 0.0
        let messageHeight = currentVisibles.message ? paddings.messageBottom + paddings.messageTop + BottomSheetState
            .messageFont.heightFor(
                text: message,
                width: WIDTH - paddings.leftRight * 2
            ) : 0.0
        let indHeight = currentVisibles.indicator ? indicatorHeight : 0.0

        height = titleHeight + buttonsHeight + messageHeight + indHeight

        self.nextAction = nextAction
    }

    mutating func updateHeight() {
        height = getCurrentHeight()
    }

    func getCurrentHeight() -> CGFloat {
        let titleHeight = currentVisibles.title ? BottomSheetState.titleFont.heightFor(
            text: title,
            width: WIDTH - paddings.leftRight * 2
        ) + paddings.titleTop + paddings.titleBottom : 0.0
        let buttonsHeight: CGFloat = currentVisibles.buttons ? 52 + 4 + paddings.buttonsTop + paddings
            .buttonsBottom : 0.0
        let messageHeight = currentVisibles.message ? paddings.messageBottom + paddings.messageTop + BottomSheetState
            .messageFont.heightFor(
                text: message,
                width: WIDTH - paddings.leftRight * 2
            ) : 0.0
        let indHeight = currentVisibles.indicator ? indicatorHeight : 0.0

        return titleHeight + messageHeight + buttonsHeight + indHeight
    }

    mutating func set(title: String) {
        self.title = title
    }

    mutating func set(message: String) {
        self.message = message
    }

    mutating func set(visibles: Visible) {
        currentVisibles = visibles
    }

    public func new(with lifeCycleState: LifeCycleState, visibles: Visible) -> BottomSheetState {
        return BottomSheetState(
            title: title,
            message: message,
            doneTitle: doneButtonTitle,
            cancelButtonTitle: cancelButtonTitle,
            paddings: paddings,
            lifeCycleState: lifeCycleState,
            successMessage: successMessage,
            errorMessage: errorMessage,
            indicatorHeight: indicatorHeight,
            currentVisibles: visibles,
            nextAction: nextAction
        )
    }

    public func newFor(success: Bool, with visibles: Visible, loaderHeight: CGFloat = 100) -> BottomSheetState {
        return BottomSheetState(
            title: title,
            message: success ? successMessage : errorMessage,
            doneTitle: "Ok",
            cancelButtonTitle: cancelButtonTitle,
            paddings: paddings,
            lifeCycleState: success ? LifeCycleState.success : LifeCycleState.error,
            successMessage: successMessage,
            errorMessage: errorMessage,
            indicatorHeight: loaderHeight,
            currentVisibles: visibles,
            nextAction: nextAction
        )
    }

    public func fromInitial(with title: String, message: String, successMessage: String, nextAction: @escaping ActionBlock) -> BottomSheetState {
        return BottomSheetState(title: title, message: message, doneTitle: BottomSheetState.initialState.doneButtonTitle, cancelButtonTitle: BottomSheetState.initialState.cancelButtonTitle, paddings: BottomSheetState.initialState.paddings, lifeCycleState: BottomSheetState.initialState.lifeCycleState, successMessage: successMessage, errorMessage: BottomSheetState.initialState.errorMessage, indicatorHeight: BottomSheetState.initialState.indicatorHeight, currentVisibles: BottomSheetState.initialState.currentVisibles, nextAction: nextAction)
    }
}

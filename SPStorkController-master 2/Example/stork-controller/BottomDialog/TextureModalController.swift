//
//  TextoreModalController.swift
//  stork-controller
//
//  Created by Лысков Павел on 17.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import AsyncDisplayKit
import RxCocoa
import RxCocoa_Texture
import RxSwift
import TextureSwiftSupport
import UIKit

final class TextureModalController: ASViewController<TextureModalContentNode>, HalfSheetPresentableProtocol/*, HalfSheetTopVCProviderProtocol*/ {
    var managedScrollView: UIScrollView? {
        return nil
    }
    
    var dismissMethod: [DismissMethod] = [.swipe, .tap]
    
    public private(set) var disposeBag = DisposeBag()
    
    var sheetHeight: CGFloat? = 500.0 {
        didSet {
            guard oldValue != sheetHeight else { return }
            
            self.didUpdateSheetHeight()
        }
    }
    
    var isLocked: Bool = false
    
    typealias Node = TextureModalController
    
    let viewModel: BottomSheetViewModel
    lazy var closeDriver: Driver<Bool> = viewModel.output.closeDriver.asDriver()
    lazy var stateDiver: Driver<BottomSheetState> = viewModel.output.currentState.asDriver()
    
    init(viewModel: BottomSheetViewModel) {
        self.viewModel = viewModel
        let targetNode = TextureModalContentNode(viewModel: viewModel)
        super.init(node: targetNode)
        targetNode.parentVC = self
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var lightStatusBar: Bool = false

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return lightStatusBar ? .lightContent : .default
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lightStatusBar = true
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    var oldState: BottomSheetState = .initialState
    
    func bindViewModel() {
      
        closeDriver.drive(onNext: { [weak self] needToClose in
            guard needToClose else { return }
            
            self?.dismiss(animated: true, completion: nil)
            
        }).disposed(by: disposeBag)
    }
    
}

extension TextureModalController: HalfSheetAppearanceProtocol {
    
    
//    var topVCTransitionStyle: HalfSheetTopVCTransitionStyle {
//        .slide
//    }
    
    var cornerRadius: CGFloat {
        return 10.0
    }
}

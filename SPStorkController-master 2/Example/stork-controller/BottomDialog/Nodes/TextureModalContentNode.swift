//
//  TextureModalContentNode.swift
//  stork-controller
//
//  Created by Лысков Павел on 17.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//
import AsyncDisplayKit
import RxCocoa
import RxCocoa_Texture
import RxSwift
import TextureSwiftSupport
import UIKit
// swiftlint:disable all
final class TextureModalContentNode: ASDisplayNode {
    struct Style {
        struct Font {
            static var title: UIFont = .boldSystemFont(ofSize: 24)
            static var message: UIFont = .systemFont(ofSize: 17)
            static var buttonTitle: UIFont = .systemFont(ofSize: 16, weight: .semibold)
        }

        struct Color {
            static var title: UIColor = {
                if #available(iOS 13.0, *) {
                    return .label
                } else {
                    return .black
                }
            }()

            static var message: UIColor = {
                if #available(iOS 13.0, *) {
                    return .secondaryLabel
                } else {
                    return .gray
                }
            }()

            static var background: UIColor = {
                if #available(iOS 13.0, *) {
                    return .systemBackground
                } else {
                    return .white
                }
            }()

            static var buttonBorder: UIColor = {
                if #available(iOS 13.0, *) {
                    return .tertiaryLabel
                } else {
                    return UIColor(white: 0.2, alpha: 1)
                }
            }()

            static var buttonTitle: UIColor = {
                UIColor.white

            }()

            static let placeHolderColor: UIColor = UIColor.gray.withAlphaComponent(0.2)
        }
    }

    lazy var titleNode = { () -> ASTextNode in

        let node = ASTextNode()
        node.placeholderColor = TextureModalContentNode.Style.Color.placeHolderColor
        return node

    }()

    lazy var messageNode = { () -> ASTextNode in

        let node = ASTextNode()
        node.placeholderColor = TextureModalContentNode.Style.Color.placeHolderColor
        return node

    }()

    lazy var indicatorNode = { () -> LoaderNode in

        let node = LoaderNode()

        return node

    }()

    lazy var indicatorSuccessNode = { () -> LoaderNode in

        let node = LoaderNode()

        return node

    }()

    lazy var doneButtonNode = configure(ASButtonNode()) {
        let title = "Next".at.attributed {
            $0.font(Style.Font.buttonTitle).foreground(color: Style.Color.buttonTitle)
        }
        $0.setAttributedTitle(title, for: .normal)
        $0.backgroundColor = UIColor.systemBlue
        $0.cornerRadius = 10
        $0.cornerRoundingType = .defaultSlowCALayer
    }

    lazy var cancelButtonNode = configure(ASButtonNode()) {
        let title = "Cancel".at.attributed {
            $0.font(Style.Font.buttonTitle).foreground(color: Style.Color.title)
        }
        $0.setAttributedTitle(title, for: .normal)

        $0.backgroundColor = Style.Color.background
        $0.cornerRadius = 10
        $0.cornerRoundingType = .defaultSlowCALayer
        $0.borderWidth = 1.0
        $0.borderColor = Style.Color.buttonBorder.cgColor
    }

    var viewModel: BottomSheetViewModel

    public private(set) var disposeBag = DisposeBag()
    lazy var stateDiver: Driver<BottomSheetState> = viewModel.output.currentState.asDriver()
    lazy var closeDriver: BehaviorRelay<Bool> = viewModel.output.closeDriver

    public var parentVC: TextureModalController?

    init(viewModel: BottomSheetViewModel) {
        self.viewModel = viewModel
        super.init()

        backgroundColor = Style.Color.background

        automaticallyManagesSubnodes = true

        bindViewModel()
    }

    override func didLoad() {
        super.didLoad()

        doneButtonNode.onDidLoad { node in
            (node as! ASButtonNode).fh.controlEnable(normalColor: .systemBlue, highlightedColor: UIColor.systemBlue.withAlphaComponent(0.7))
        }

        cancelButtonNode.onDidLoad { node in
            (node as! ASButtonNode).fh.controlEnable(normalColor: Style.Color.background, highlightedColor: Style.Color.buttonBorder.withAlphaComponent(0.5))
        }
    }

    override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
        return calcSpec(for: viewModel.output.currentState.value)
    }

    func calcSpec(for state: BottomSheetState) -> ASLayoutSpec {
        let commonSpec: ASLayoutSpec = {
            LayoutSpec {
                ZStackLayout {
                    VStackLayout {
                        self.titleNode
                            .padding(UIEdgeInsets(
                                top: state.paddings.titleTop,
                                left: 0,
                                bottom: state.paddings.titleBottom,
                                right: 0
                            ))
                        self.messageNode
                            .padding(UIEdgeInsets(
                                top: state.paddings.messageTop,
                                left: 0,
                                bottom: state.paddings.messageBottom,
                                right: 0
                            ))
                    }
                    .padding(UIEdgeInsets(
                        top: 0,
                        left: state.paddings.leftRight,
                        bottom: CGFloat.infinity,
                        right: state.paddings.leftRight
                    ))
                    HStackLayout(spacing: 16) {
                        self.cancelButtonNode.width(120).height(52)
                        self.doneButtonNode.height(52).flexGrow(1.0)
                    }.height(52)
                        .padding(UIEdgeInsets(
                            top: CGFloat.infinity,
                            left: state.paddings.leftRight,
                            bottom: state.paddings.buttonsBottom,
                            right: state.paddings.leftRight
                        ))
                }
            }
        }()

        let finishSpec: ASLayoutSpec = {
            LayoutSpec {
                ZStackLayout {
                    VStackLayout {
                        self.titleNode
                            .padding(UIEdgeInsets(
                                top: state.paddings.titleTop,
                                left: 0,
                                bottom: state.paddings.titleBottom,
                                right: 0
                            ))
                        CenterLayout(centeringOptions: .X, sizingOptions: []) {
                            self.indicatorSuccessNode.height(state.indicatorHeight).width(state.indicatorHeight)
                        }
                        CenterLayout(centeringOptions: .X, sizingOptions: []) {
                            self.messageNode
                                .padding(UIEdgeInsets(
                                    top: state.paddings.messageTop,
                                    left: CGFloat.infinity,
                                    bottom: state.paddings.messageBottom,
                                    right: CGFloat.infinity
                                ))
                        }
                    }
                    .padding(UIEdgeInsets(
                        top: 0,
                        left: state.paddings.leftRight,
                        bottom: CGFloat.infinity,
                        right: state.paddings.leftRight
                    ))
                    HStackLayout(spacing: 0) {
                        self.doneButtonNode.height(52)
                    }
                    .padding(UIEdgeInsets(
                        top: CGFloat.infinity,
                        left: state.paddings.leftRight,
                        bottom: state.paddings.buttonsBottom,
                        right: state.paddings.leftRight
                    ))
                }
            }
        }()

        switch state.lifeCycleState {
        case .initial:
            return commonSpec
        case .waitingForResponse:
            return LayoutSpec {
                ZStackLayout {
                    VStackLayout {
                        self.titleNode
                            .padding(UIEdgeInsets(
                                top: state.paddings.titleTop,
                                left: 0,
                                bottom: state.paddings.titleBottom,
                                right: 0
                            ))
                        CenterLayout(centeringOptions: .X, sizingOptions: []) {
                            self.indicatorNode.height(state.indicatorHeight).width(state.indicatorHeight)
                        }
                    }
                    .padding(UIEdgeInsets(
                        top: 0,
                        left: state.paddings.leftRight,
                        bottom: CGFloat.infinity,
                        right: state.paddings.leftRight
                    ))
                    HStackLayout(spacing: 16) {
                        self.cancelButtonNode.width(120).height(52)
                        self.doneButtonNode.height(52).flexGrow(1.0)
                    }
                    .padding(UIEdgeInsets(
                        top: CGFloat.infinity,
                        left: state.paddings.leftRight,
                        bottom: state.paddings.buttonsBottom,
                        right: state.paddings.leftRight
                    ))
                }
            }
        case .success:
            return finishSpec

        case .error:
            return finishSpec
        }
    }

    var oldState: BottomSheetState?

    func bindViewModel() {
        stateDiver.drive(onNext: { [weak self] state in
            guard let self = self else { return }

            if let old = self.oldState, old == state {
                return
            }

            self.titleNode.attributedText = state.title.at.attributed {
                $0.font(Style.Font.title).foreground(color: Style.Color.title)
            }

            self.messageNode.attributedText = state.message.at.attributed {
                $0.font(Style.Font.message).foreground(color: Style.Color.message)
            }

            let titleDone = state.doneButtonTitle.at.attributed {
                $0.font(Style.Font.buttonTitle).foreground(color: Style.Color.buttonTitle)
            }
            self.doneButtonNode.setAttributedTitle(titleDone, for: .normal)

            let titleCancel = state.cancelButtonTitle.at.attributed {
                $0.font(Style.Font.buttonTitle).foreground(color: Style.Color.title)
            }
            self.cancelButtonNode.setAttributedTitle(titleCancel, for: .normal)

            self.resolve(state: state)

            self.multiLayout(state: state)

            self.oldState = state

        }).disposed(by: disposeBag)

        onDidLoad { [weak self] _ in
            guard let self = self else { return }

            self.doneButtonNode.rx.tap
                .withLatestFrom(self.stateDiver.asObservable())
                .bind(to: self.viewModel.input.nextTrigger)
                .disposed(by: self.disposeBag)

            self.cancelButtonNode.rx.tap
                .bind(to: self.viewModel.input.closeTrigger)
                .disposed(by: self.disposeBag)
        }
    }

    func bindTap() {
        if doneButtonNode.isNodeLoaded {
            doneButtonNode.rx.tap.asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let self = self else { return }

                    self.closeDriver.accept(true)
                }).disposed(by: disposeBag)
        }
    }

    func multiLayout(state: BottomSheetState) {
        guard state != .initialState, let vc = self.parentVC else { return }

        CATransaction.begin()

        transitionLayout(withAnimation: true, shouldMeasureAsync: false, measurementCompletion: nil)

        DispatchQueue.main.async {
            vc.sheetHeight = state.getCurrentHeight()
        }

        CATransaction.commit()
    }

    func resolve(state: BottomSheetState) {
        switch state.lifeCycleState {
        case .initial:
            print("current state is initial")
        case .waitingForResponse:
            print("current state is waitingForResponse")

            if let vc = parentVC {
                vc.isLocked = true
            }

            DispatchQueue.main.async {
                self.indicatorNode.start()
                self.cancelButtonNode.set(enabled: false)
                self.doneButtonNode.set(enabled: false)
            }

            delay(4, closure: { [weak self] in
                guard let self = self else { return }
                let v = BottomSheetState.Visible(title: true, message: true, indicator: true, buttons: true, cancelButton: false)
                self.viewModel.output.currentState.accept(state.newFor(success: false, with: v, loaderHeight: 150))
                self.doneButtonNode.set(enabled: true)
                if let vc = self.parentVC {
                    vc.isLocked = false
                }
            })
        case .success:

            DispatchQueue.main.async {
                self.indicatorNode.stop()
                self.indicatorSuccessNode.success()
            }

            bindTap()
        case .error:

            DispatchQueue.main.async {
                self.indicatorNode.stop()
                self.indicatorSuccessNode.errorAnimation()
            }

            bindTap()
        }
    }
}

extension TextureModalContentNode: LayoutTransitionable {
    var presentedNodes: [ASDisplayNode] {
        switch viewModel.output.currentState.value.lifeCycleState {
        case .initial:
            return [titleNode, messageNode, doneButtonNode, cancelButtonNode]
        case .waitingForResponse:
            return [indicatorNode]
        case .success, .error:
            return [messageNode, indicatorSuccessNode]
        }
    }

    var removedNodes: [ASDisplayNode] {
        switch viewModel.output.currentState.value.lifeCycleState {
        case .initial:
            return []
        case .waitingForResponse:
            return [messageNode]
        case .success, .error:
            return [cancelButtonNode, indicatorNode]
        }
    }

    var retriverNodes: [ASDisplayNode] {
        switch viewModel.output.currentState.value.lifeCycleState {
        case .initial:
            return []
        case .waitingForResponse:
            return [titleNode, doneButtonNode, cancelButtonNode]
        case .success, .error:
            return [titleNode, doneButtonNode]
        }
    }
}

extension TextureModalContentNode {
    override func animateLayoutTransition(_ context: ASContextTransitioning) {
        let delay: TimeInterval = 0.0

        let transform = CATransform3DScale(CATransform3DIdentity, 0.88, 0.88, 1.0)

        let completeRet: BehaviorRelay<Bool> = .init(value: false)
        let completeAppear: BehaviorRelay<Bool> = .init(value: false)
        let completeDismiss: BehaviorRelay<Bool> = .init(value: false)

        let timing = UISpringTimingParameters(damping: 0.75, response: 0.35)
        let animator = UIViewPropertyAnimator(duration: 0, timingParameters: timing)
        let timingApear = UISpringTimingParameters(damping: 1.0, response: 0.35)
        let animatorApear = UIViewPropertyAnimator(duration: 0, timingParameters: timingApear)
        let timingDismiss = UISpringTimingParameters(damping: 1.0, response: 0.217)
        let animatorDismiss = UIViewPropertyAnimator(duration: 0, timingParameters: timingDismiss)

        presentedNodes.forEach {
            $0.transform = transform
            $0.alpha = 0
        }

        removedNodes.forEach {
            $0.transform = CATransform3DIdentity
            $0.alpha = 1
        }

        for node in removedNodes {
            let anim = {
                node.transform = transform
                node.alpha = 0
            }
            animatorDismiss.addAnimations(anim)
        }

        for node in retriverNodes {
            let anim = {
                node.frame = context.finalFrame(for: node)
            }
            animator.addAnimations(anim)
        }

        for node in presentedNodes {
            let anim = {
                node.alpha = 1
                node.transform = CATransform3DIdentity
            }

            animatorApear.addAnimations(anim)
        }

        animator.addCompletion { _ in
            completeRet.accept(true)
        }
        animator.startAnimation(afterDelay: delay)

        animatorApear.addCompletion { _ in
            completeAppear.accept(true)
        }
        animatorApear.startAnimation(afterDelay: delay)

        animatorDismiss.addCompletion { _ in
            completeDismiss.accept(true)
        }
        animatorDismiss.startAnimation(afterDelay: delay)

        Observable.combineLatest(completeRet.asObservable(), completeAppear.asObservable(), completeDismiss.asObservable())
            .subscribe(onNext: { ret, appear, dismiss in

                guard ret, appear, dismiss else {
                    return
                }

                context.completeTransition(true)

            }).disposed(by: disposeBag)
    }
}

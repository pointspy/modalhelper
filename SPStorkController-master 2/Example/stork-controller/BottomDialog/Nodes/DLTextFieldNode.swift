import AsyncDisplayKit
import RxCocoa
import RxCocoa_Texture
import RxSwift
// swiftlint:disable all
/// The Node object of text field view with right control node
open class DLTextFieldNode: ASDisplayNode {
    var disposeBag = DisposeBag()
    
    public var leftViewMode: UITextField.ViewMode {
        get {
            return self.originalNode.nodeView.leftViewMode
        }
        set {
            self.originalNode.nodeView.leftViewMode = newValue
        }
    }
    
    public var rightViewMode: UITextField.ViewMode {
        get {
            return self.originalNode.nodeView.rightViewMode
        }
        set {
            self.originalNode.nodeView.rightViewMode = newValue
        }
    }
    
    public let originalNode: DLSimpleTextFieldNode
    public var rightButtonNode: ASButtonNode?
    public var spacingBeforeRight: CGFloat = 8
    public var validColor: UIColor = .systemBlue
    public var highlightColor: UIColor = {
        if #available(iOS 13.0, *) {
            return .tertiaryLabel
        } else {
            return UIColor(white: 0.2, alpha: 1)
        }
    }()
    
    public var insideInsets: UIEdgeInsets {
        get {
            return self.originalNode.textInsets
        }
        set {
            self.originalNode.textInsets = newValue
        }
    }
    
    public var outsideInsets = UIEdgeInsets.zero
    
    public var isValidated: Bool {
        return self.originalNode.isValidated
    }
    
    public var text: String? {
        get {
            return self.originalNode.text
        }
        set {
            self.originalNode.text = newValue
        }
    }
    
    public init(iconName: String? = nil, placeholder: String = "", maxLength: UInt = 32, isSecure: Bool = false, clearMode: UITextField.ViewMode = .always, keyboardType: UIKeyboardType? = nil, returnKeyType: UIReturnKeyType? = nil) {
        self.originalNode = DLSimpleTextFieldNode(iconName: iconName, placeholder: placeholder, maxLength: maxLength, isSecure: isSecure, clearMode: clearMode, keyboardType: keyboardType, returnKeyType: returnKeyType)
        super.init()
        
        self.automaticallyManagesSubnodes = true
    }
    
    public init(iconImage: UIImage? = nil, placeholder: String, maxLength: UInt = 32, isSecure: Bool = false, clearMode: UITextField.ViewMode = .always, keyboardType: UIKeyboardType? = nil, returnKeyType: UIReturnKeyType? = nil) {
        self.originalNode = DLSimpleTextFieldNode(iconImage: iconImage, placeholder: placeholder, maxLength: maxLength, isSecure: isSecure, clearMode: clearMode, keyboardType: keyboardType, returnKeyType: returnKeyType)
        super.init()
        
        self.automaticallyManagesSubnodes = true
    }
    
    open override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        var children = [ASLayoutElement](arrayLiteral: originalNode)
        if let rightButtonNode = rightButtonNode {
            rightButtonNode.style.spacingAfter = self.outsideInsets.right
            children.append(rightButtonNode)
        } else {
            self.originalNode.style.spacingAfter = self.outsideInsets.right / 2
        }
        let horizontalStackSpec = ASStackLayoutSpec(direction: .horizontal, spacing: spacingBeforeRight, justifyContent: .start, alignItems: .center, children: children)
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: self.outsideInsets.top, left: self.outsideInsets.left, bottom: self.outsideInsets.bottom, right: 0), child: horizontalStackSpec)
    }
    
    open override func becomeFirstResponder() -> Bool {
        return self.originalNode.becomeFirstResponder()
    }
    
    open override func resignFirstResponder() -> Bool {
        return self.originalNode.resignFirstResponder()
    }
    
    open override func didLoad() {
        super.didLoad()
        
        let originalColor = self.borderColor
        
        self.originalNode.nodeView.rx.controlEvent(.editingDidBegin)
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                
                self.borderColor = self.highlightColor.cgColor
                
            }).disposed(by: self.disposeBag)
        
        self.originalNode.nodeView.rx.controlEvent(.editingDidEnd)
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                
                self.borderColor = originalColor
                
            }).disposed(by: self.disposeBag)
    }
}

/// The Node objcet of simple text field view
open class DLSimpleTextFieldNode: DLViewNode<DLTextField> {
    public var isEditing: Bool {
        return self.nodeView.isEditing
    }
    
    public var isSecureTextEntry: Bool {
        get {
            return self.nodeView.isSecureTextEntry
        }
        set {
            self.appendViewAssociation { (view: DLTextField) in
                view.isSecureTextEntry = newValue
            }
        }
    }
    
    public var text: String? {
        get {
            return self.nodeView.text
        }
        set {
            self.appendViewAssociation { (view: DLTextField) in
                view.text = newValue
            }
        }
    }
    
    public var font: UIFont? {
        get {
            return self.nodeView.font
        }
        set {
            self.appendViewAssociation { (view: DLTextField) in
                view.font = newValue
            }
        }
    }
    
    public var textColor: UIColor? {
        get {
            return self.nodeView.textColor
        }
        set {
            self.appendViewAssociation { (view: DLTextField) in
                view.textColor = newValue
            }
        }
    }
    
    public var textSize: CGFloat? {
        didSet {
            let _textSize = self.textSize!
            self.appendViewAssociation { (view: DLTextField) in
                view.font = UIFont.systemFont(ofSize: _textSize)
            }
        }
    }
    
    public var textInsets: UIEdgeInsets {
        get {
            return self.nodeView.textInsets
        }
        set {
            self.appendViewAssociation { (view: DLTextField) in
                view.textInsets = newValue
            }
        }
    }
    
    public var leftInset: CGFloat {
        get {
            return self.nodeView.leftInset
        }
        set {
            self.appendViewAssociation { (view: DLTextField) in
                view.leftInset = newValue
            }
        }
    }
    
    public var isValidated: Bool {
        var isValidated = validation()
        if isValidated, let appendedValidation = _appendedValidation {
            isValidated = isValidated && appendedValidation()
        }
        if !isValidated {
            //            _ = self.becomeFirstResponder() // FIXME: cause endless loop in iOS 11
        }
        return isValidated
    }
    
    open var validation: () -> Bool {
        return { () -> Bool in
            guard let text = self.text, !text.isEmpty else {
                return false
            }
            
            return true
        }
    }
    
    open func setIconWith(image: UIImage) {
        self.nodeView.leftView = UIImageView(image: image)
    }
    
    open func setRightIconWith(image: UIImage) {
        self.nodeView.rightView = UIImageView(image: image)
    }
    
    var leftView: UIView? {
        return self.nodeView.leftView
    }
    
    var rightView: UIView? {
        return self.nodeView.rightView
    }
    
    private var _appendedValidation: (() -> Bool)?
    
    public convenience init(iconName: String? = nil, placeholder: String, maxLength: UInt = 32, isSecure: Bool = false, clearMode: UITextField.ViewMode = .always, keyboardType: UIKeyboardType? = nil, returnKeyType: UIReturnKeyType? = nil) {
        if let iconName = iconName {
            self.init(iconImage: UIImage.as_imageNamed(iconName), placeholder: placeholder, maxLength: maxLength, isSecure: isSecure, clearMode: clearMode, keyboardType: keyboardType, returnKeyType: returnKeyType)
        } else {
            self.init(iconImage: nil, placeholder: placeholder, maxLength: maxLength, isSecure: isSecure, clearMode: clearMode, keyboardType: keyboardType, returnKeyType: returnKeyType)
        }
    }
    
    public init(iconImage: UIImage? = nil, placeholder: String, maxLength: UInt = 32, isSecure: Bool = false, clearMode: UITextField.ViewMode = .always, keyboardType: UIKeyboardType? = nil, returnKeyType: UIReturnKeyType? = nil) {
        super.init()
        
        self.setViewBlock { () -> UIView in
            let textField = DLTextField()
            
            if let iconImage = iconImage {
                textField.leftView = UIImageView(image: iconImage)
                textField.leftViewMode = .always
            }
            
            textField.placeholder = placeholder
            textField.maxLength = maxLength
            
            textField.clearButtonMode = clearMode
            if isSecure {
                textField.clearButtonMode = .never
                textField.isSecureTextEntry = true
                textField.keyboardAppearance = .dark
            }
            
            if let keyboardType = keyboardType {
                textField.keyboardType = keyboardType
            }
            
            if let returnKeyType = returnKeyType {
                textField.returnKeyType = returnKeyType
            }
            return textField
        }
        self.style.alignSelf = .stretch
        self.style.flexGrow = 1.0
        self.style.flexShrink = 1.0
    }
    
    public func appendValidation(_ block: @escaping () -> Bool) {
        self._appendedValidation = block
    }
}

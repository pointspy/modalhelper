//
//  DLViewNode.swift
//  NodeExtension
//
//  Created by Linzh on 8/28/17.
//  Copyright (c) 2017 Daniel Lin. All rights reserved.
//

import AsyncDisplayKit
// swiftlint:disable all
open class DLViewNode<ViewType: UIView>: ASDisplayNode {
    private var _viewAssociations: [(ViewType) -> Void]?
    
    public var nodeView: ViewType {
        return view as! ViewType
    }
    
    public func appendViewAssociation(block: @escaping (ViewType) -> Void) {
        if isNodeLoaded {
            block(nodeView)
        } else {
            if _viewAssociations == nil {
                _viewAssociations = [(ViewType) -> Void]()
            }
            _viewAssociations!.append(block)
        }
    }
    
    open override func didLoad() {
        super.didLoad()
        
        viewAssociationWhenNodeLoaded()
    }
    
    private func viewAssociationWhenNodeLoaded() {
        guard let viewAssociations = _viewAssociations else {
            return
        }
        
        for block in viewAssociations {
            block(nodeView)
        }
        
        _viewAssociations = nil
    }
    
    override init() {
        super.init()
        
        setViewBlock { () -> ViewType in
            let view = ViewType()
            
            return view
        }
        
        // FIXME: fit size
        
        style.flexShrink = 1
        style.flexGrow = 1
        
        self.backgroundColor = .clear
    }
    
    public init(frame: CGRect) {
        super.init()
        
        setViewBlock { () -> ViewType in
            let view = ViewType(frame: frame)
            
            return view
        }
        
        // FIXME: fit size
        style.preferredSize = frame.size
        
        self.backgroundColor = .clear
    }
}

//
//  ButtonCustomEnabledNode.swift
//  stork-controller
//
//  Created by Pavel Lyskov on 25.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import AsyncDisplayKit

final class ButtonCustomEnabledNode: ASButtonNode {
    
    override var isEnabled: Bool {
        
        get {
            guard self.isNodeLoaded else {return false}
            
            return super.isEnabled
            
        }
        
        set {
            guard self.isNodeLoaded else {return}
            super.set(enabled: newValue)
        }
        
    }
    
}

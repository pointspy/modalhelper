// swiftlint:disable all
//
//  TextureModalContentNode.swift
//  stork-controller
//
//  Created by Лысков Павел on 17.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//
import AsyncDisplayKit
import RxCocoa
import RxCocoa_Texture
import RxKeyboard
import RxSwift
import TextureSwiftSupport
import UIKit

final class TextureModalLoginContentNode: ASDisplayNode {
    struct Style {
        struct Font {
            static var title: UIFont = .boldSystemFont(ofSize: 24)
            static var message: UIFont = .systemFont(ofSize: 17)
            static var buttonTitle: UIFont = .systemFont(ofSize: 16, weight: .semibold)
        }

        struct Color {
            static var title: UIColor = {
                if #available(iOS 13.0, *) {
                    return .label
                } else {
                    return .black
                }
            }()

            static var message: UIColor = {
                if #available(iOS 13.0, *) {
                    return .secondaryLabel
                } else {
                    return .gray
                }
            }()

            static var background: UIColor = {
                if #available(iOS 13.0, *) {
                    return .systemBackground
                } else {
                    return .white
                }
            }()

            static var buttonBorder: UIColor = {
                if #available(iOS 13.0, *) {
                    return .tertiaryLabel
                } else {
                    return UIColor(white: 0.2, alpha: 1)
                }
            }()

            static var buttonTitle: UIColor = {
                UIColor.white

            }()

            static let placeHolderColor: UIColor = UIColor.gray.withAlphaComponent(0.2)
        }
    }

    lazy var titleNode = { () -> ASTextNode in

        let node = ASTextNode()
        node.placeholderColor = TextureModalContentNode.Style.Color.placeHolderColor
        return node

    }()

    lazy var messageNode = { () -> ASTextNode in

        let node = ASTextNode()
        node.placeholderColor = TextureModalContentNode.Style.Color.placeHolderColor
        return node

    }()

    lazy var indicatorNode = { () -> LoaderNode in

        let node = LoaderNode()

        return node

    }()

    lazy var indicatorSuccessNode = { () -> LoaderNode in

        let node = LoaderNode()

        return node

    }()

    lazy var doneButtonNode = configure(ASButtonNode()) {
        let title = "Next".at.attributed {
            $0.font(Style.Font.buttonTitle).foreground(color: Style.Color.buttonTitle)
        }
        $0.setAttributedTitle(title, for: .normal)
        $0.backgroundColor = UIColor.systemBlue
        $0.cornerRadius = 10
        $0.cornerRoundingType = .defaultSlowCALayer
    }

    lazy var loginTextNode: TextFieldWithIconNode = TextFieldWithIconNode(imageName: "user", placeholder: "логин", isSecurity: false)
    lazy var passwordTextNode: TextFieldWithIconNode = TextFieldWithIconNode(imageName: "password", placeholder: "пароль", isSecurity: true)

    var viewModel: BottomSheetLoginViewModel

    public private(set) var disposeBag = DisposeBag()
    lazy var stateDiver: Driver<BottomSheetLoginState> = viewModel.output.currentState.asDriver()
    lazy var closeDriver: BehaviorRelay<Bool> = viewModel.output.closeDriver

    public var parentVC: TextureModalLoginController?

    private var isKeyboardUp: BehaviorRelay<Bool> = .init(value: false)

    private var stateRelay: BehaviorRelay<BottomSheetLoginState> = .init(value: .initialState)

    init(viewModel: BottomSheetLoginViewModel) {
        self.viewModel = viewModel
        super.init()

        backgroundColor = Style.Color.background

        automaticallyManagesSubnodes = true

        bindViewModel()
    }

    override func didLoad() {
        super.didLoad()

        doneButtonNode.onDidLoad { node in
            (node as! ASButtonNode).fh.controlEnable(normalColor: .systemBlue, highlightedColor: UIColor.systemBlue.withAlphaComponent(0.7))
        }

        if let vc = self.parentVC {
            vc.sheetHeight = stateRelay.value.getCurrentHeight()

            RxKeyboard.instance.willShowVisibleHeight
                .throttle(.milliseconds(300))
                .drive(onNext: { [weak self] newHeight in
                    guard let self = self else { return }
                    vc.sheetHeight = self.stateRelay.value.getCurrentHeight() + newHeight
                }).disposed(by: disposeBag)

            RxKeyboard.instance.isHidden
                .throttle(.milliseconds(300))
                .drive(onNext: { [weak self] hidden in
                    guard let self = self, hidden else { return }
                    vc.sheetHeight = self.stateRelay.value.getCurrentHeight()
                }).disposed(by: disposeBag)
        }

        let rec = UITapGestureRecognizer(target: self, action: #selector(tap))
        view.addGestureRecognizer(rec)

        loginTextNode.textDriver.asDriver().drive(onNext: { v in
            print("vvvvv   \(v)")
        }).disposed(by: loginTextNode.disposeBag)
    }

    @objc
    func tap() {
        view.endEditing(true)
    }

    override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
        return calcSpec(for: stateRelay.value)
    }

    func calcSpec(for state: BottomSheetLoginState) -> ASLayoutSpec {
        let commonSpec: ASLayoutSpec = {
            LayoutSpec {
                ZStackLayout {
                    VStackLayout {
                        self.titleNode
                            .padding(UIEdgeInsets(
                                top: state.paddings.titleTop,
                                left: 0,
                                bottom: state.paddings.titleBottom,
                                right: 0
                            ))
                        self.messageNode
                            .padding(UIEdgeInsets(
                                top: state.paddings.messageTop,
                                left: 0,
                                bottom: state.paddings.messageBottom,
                                right: 0
                            ))
                        self.loginTextNode.height(state.paddings.textFieldHeight).padding(UIEdgeInsets(top: state.paddings.loginTop, left: 0, bottom: 0, right: 0))
                        self.passwordTextNode.height(state.paddings.textFieldHeight).padding(UIEdgeInsets(top: state.paddings.passwordTop, left: 0, bottom: 0, right: 0))
                    }
                    .padding(UIEdgeInsets(
                        top: 0,
                        left: state.paddings.leftRight,
                        bottom: CGFloat.infinity,
                        right: state.paddings.leftRight
                    ))
                    HStackLayout(spacing: 16) {
                        self.doneButtonNode.height(52).flexGrow(1.0)
                    }.height(52)
                        .padding(UIEdgeInsets(
                            top: CGFloat.infinity,
                            left: state.paddings.leftRight,
                            bottom: state.paddings.buttonsBottom,
                            right: state.paddings.leftRight
                        ))
                }
            }
        }()

        let finishSpec: ASLayoutSpec = {
            LayoutSpec {
                ZStackLayout {
                    VStackLayout {
                        self.titleNode
                            .padding(UIEdgeInsets(
                                top: state.paddings.titleTop,
                                left: 0,
                                bottom: state.paddings.titleBottom,
                                right: 0
                            ))
                        CenterLayout(centeringOptions: .X, sizingOptions: []) {
                            self.indicatorSuccessNode.height(state.indicatorHeight).width(state.indicatorHeight)
                        }.padding(UIEdgeInsets(
                            top: state.paddings.indicatorTop,
                            left: 0,
                            bottom: state.paddings.indicatorBottom,
                            right: 0
                        ))
                        CenterLayout(centeringOptions: .X, sizingOptions: []) {
                            self.messageNode
                                .padding(UIEdgeInsets(
                                    top: state.paddings.messageTop,
                                    left: CGFloat.infinity,
                                    bottom: state.paddings.messageBottom,
                                    right: CGFloat.infinity
                                ))
                        }
                    }
                    .padding(UIEdgeInsets(
                        top: 0,
                        left: state.paddings.leftRight,
                        bottom: CGFloat.infinity,
                        right: state.paddings.leftRight
                    ))
                    HStackLayout(spacing: 0) {
                        self.doneButtonNode.height(52)
                    }
                    .padding(UIEdgeInsets(
                        top: CGFloat.infinity,
                        left: state.paddings.leftRight,
                        bottom: state.paddings.buttonsBottom,
                        right: state.paddings.leftRight
                    ))
                }
            }
        }()

        switch state.lifeCycleState {
        case .initial:
            return commonSpec
        case .waitingForResponse:
            return LayoutSpec {
                ZStackLayout {
                    VStackLayout {
                        self.titleNode
                            .padding(UIEdgeInsets(
                                top: state.paddings.titleTop,
                                left: 0,
                                bottom: state.paddings.titleBottom,
                                right: 0
                            ))
                        CenterLayout(centeringOptions: .X, sizingOptions: []) {
                            self.indicatorNode.height(state.indicatorHeight).width(state.indicatorHeight)
                        }.padding(UIEdgeInsets(
                            top: state.paddings.indicatorTop,
                            left: 0,
                            bottom: state.paddings.indicatorBottom,
                            right: 0
                        ))
                    }
                    .padding(UIEdgeInsets(
                        top: 0,
                        left: state.paddings.leftRight,
                        bottom: CGFloat.infinity,
                        right: state.paddings.leftRight
                    ))
                    HStackLayout(spacing: 16) {
                        self.doneButtonNode.height(52).flexGrow(1.0)
                    }
                    .padding(UIEdgeInsets(
                        top: CGFloat.infinity,
                        left: state.paddings.leftRight,
                        bottom: state.paddings.buttonsBottom,
                        right: state.paddings.leftRight
                    ))
                }
            }
        case .success:
            return finishSpec
        }
    }

    var oldState: BottomSheetLoginState?

    var run: BehaviorRelay<Void> = .init(value: {}())

    func bindViewModel() {
        stateDiver.drive(stateRelay)
            .disposed(by: disposeBag)

        stateDiver.drive(onNext: { [weak self] state in
            guard let self = self else { return }

            if let old = self.oldState, old == state {
                return
            }

            self.titleNode.attributedText = state.title.at.attributed {
                $0.font(Style.Font.title).foreground(color: Style.Color.title)
            }

            self.messageNode.attributedText = state.message

            let titleDone = state.doneButtonTitle.at.attributed {
                $0.font(Style.Font.buttonTitle).foreground(color: Style.Color.buttonTitle)
            }
            self.doneButtonNode.setAttributedTitle(titleDone, for: .normal)

            self.resolve(state: state)

            self.multiLayout(state: state)

            self.oldState = state

        }).disposed(by: disposeBag)

        onDidLoad { [weak self] _ in
            guard let self = self else { return }

            self.doneButtonNode.set(enabled: false)

            let loginTextDriver = self.loginTextNode.fieldNode.nodeView.rx.text.asDriver()
                .flatMapLatest { text -> Driver<String> in
                    guard let text = text else {
                        return BehaviorRelay<String>.init(value: "").asDriver()
                    }

                    return BehaviorRelay<String>.init(value: text).asDriver()
                }.asDriver()

            let passwordTextDriver = self.passwordTextNode.fieldNode.nodeView.rx.text.asDriver()
                .flatMapLatest { text -> Driver<String> in
                    guard let text = text else {
                        return BehaviorRelay<String>.init(value: "").asDriver()
                    }

                    return BehaviorRelay<String>.init(value: text).asDriver()
                }.asDriver()

            let loginPasswordObservable: Observable<(String, String)> = Driver.combineLatest(loginTextDriver, passwordTextDriver).flatMapLatest { value -> Driver<(String, String)> in
                BehaviorRelay(value: value).asDriver()
            }.asObservable()

            self.doneButtonNode.rx.tap
                .withLatestFrom(loginPasswordObservable)
                .bind(to: self.viewModel.input.nextTrigger)
                .disposed(by: self.disposeBag)

            Driver.combineLatest(self.loginTextNode.isValidRelay.asDriver(), self.passwordTextNode.isValidRelay.asDriver())
                .flatMapLatest { value -> Driver<Bool> in
                    BehaviorRelay<Bool>.init(value: value.0 && value.1).asDriver()
                }.asDriver()
                .drive(onNext: { enabled in
                    self.doneButtonNode.set(enabled: enabled)
                }).disposed(by: self.disposeBag)
        }
    }

    func bindTap() {
        if doneButtonNode.isNodeLoaded {
            doneButtonNode.rx.tap.asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let self = self else { return }

                    self.closeDriver.accept(true)
                }).disposed(by: disposeBag)
        }
    }

    func multiLayout(state: BottomSheetLoginState) {
        guard state != .initialState, let vc = self.parentVC else { return }

        CATransaction.begin()

        transitionLayout(withAnimation: true, shouldMeasureAsync: false, measurementCompletion: nil)

        DispatchQueue.main.async {
            vc.sheetHeight = state.getCurrentHeight()
        }

        CATransaction.commit()
    }

    func resolve(state: BottomSheetLoginState) {
        switch state.lifeCycleState {
        case .initial:
            if let vc = parentVC {
                vc.isLocked = true
            }
            DispatchQueue.main.async {
                self.indicatorNode.stop()
            }

            if state.isError {
                loginTextNode.isErrorRelay.accept(true)
                passwordTextNode.isErrorRelay.accept(true)
                doneButtonNode.set(enabled: true)
            }

            print("current state is initial")
        case .waitingForResponse:
            print("current state is waitingForResponse")

            view.endEditing(true)

            if let vc = parentVC {
                vc.isLocked = true
            }

            DispatchQueue.main.async {
                self.indicatorNode.start()
                self.doneButtonNode.set(enabled: false)
            }

        case .success:

            if let vc = parentVC {
                vc.isLocked = false
            }

            DispatchQueue.main.async {
                self.indicatorNode.stop()
                self.indicatorSuccessNode.success()
                self.doneButtonNode.set(enabled: true)
            }
        }
    }
}

extension TextureModalLoginContentNode: LayoutTransitionable {
    var presentedNodes: [ASDisplayNode] {
        switch stateRelay.value.lifeCycleState {
        case .initial:
            return [messageNode, loginTextNode, passwordTextNode]
        case .waitingForResponse:
            return [indicatorNode]
        case .success:
            return [messageNode, indicatorSuccessNode]
        }
    }

    var removedNodes: [ASDisplayNode] {
        switch stateRelay.value.lifeCycleState {
        case .initial:
            return [indicatorNode]
        case .waitingForResponse:
            return [messageNode, loginTextNode, passwordTextNode]
        case .success:
            return [indicatorNode]
        }
    }

    var retriverNodes: [ASDisplayNode] {
        switch stateRelay.value.lifeCycleState {
        case .initial:
            return [titleNode, doneButtonNode]
        case .waitingForResponse:
            return [titleNode, doneButtonNode]
        case .success:
            return [titleNode, doneButtonNode]
        }
    }
}

extension TextureModalLoginContentNode {
    override func animateLayoutTransition(_ context: ASContextTransitioning) {
        let delay: TimeInterval = 0.0

        let transform = CATransform3DScale(CATransform3DIdentity, 0.88, 0.88, 1.0)

        let completeRet: BehaviorRelay<Bool> = .init(value: false)
        let completeAppear: BehaviorRelay<Bool> = .init(value: false)
        let completeDismiss: BehaviorRelay<Bool> = .init(value: false)

        let timing = UISpringTimingParameters(damping: 0.75, response: 0.35)
        let animator = UIViewPropertyAnimator(duration: 0, timingParameters: timing)
        let timingApear = UISpringTimingParameters(damping: 1.0, response: 0.35)
        let animatorApear = UIViewPropertyAnimator(duration: 0, timingParameters: timingApear)
        let timingDismiss = UISpringTimingParameters(damping: 1.0, response: 0.217)
        let animatorDismiss = UIViewPropertyAnimator(duration: 0, timingParameters: timingDismiss)

        presentedNodes.forEach {
            $0.transform = transform
            $0.alpha = 0
        }

        removedNodes.forEach {
            $0.transform = CATransform3DIdentity
            $0.alpha = 1
        }

        for node in removedNodes {
            let anim = {
                node.transform = transform
                node.alpha = 0
            }
            animatorDismiss.addAnimations(anim)
        }

        for node in retriverNodes {
            let anim = {
                node.frame = context.finalFrame(for: node)
            }
            animator.addAnimations(anim)
        }

        for node in presentedNodes {
            let anim = {
                node.alpha = 1
                node.transform = CATransform3DIdentity
            }

            animatorApear.addAnimations(anim)
        }

        animator.addCompletion { _ in
            completeRet.accept(true)
        }
        animator.startAnimation(afterDelay: delay)

        animatorApear.addCompletion { _ in
            completeAppear.accept(true)
        }
        animatorApear.startAnimation(afterDelay: delay)

        animatorDismiss.addCompletion { _ in
            completeDismiss.accept(true)
        }
        animatorDismiss.startAnimation(afterDelay: delay)

        Observable.combineLatest(completeRet.asObservable(), completeAppear.asObservable(), completeDismiss.asObservable())
            .subscribe(onNext: { ret, appear, dismiss in

                guard ret, appear, dismiss else {
                    return
                }

                context.completeTransition(true)

            }).disposed(by: disposeBag)
    }
}

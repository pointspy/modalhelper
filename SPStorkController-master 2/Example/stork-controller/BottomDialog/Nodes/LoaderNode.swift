//
//  LoaderNode.swift
//  stork-controller
//
//  Created by Pavel Lyskov on 19.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import AsyncDisplayKit
// swiftlint:disable all
final class LoaderNode: DLViewNode<LottieLoaderView> {
    var loader: LottieLoaderView {
        return self.nodeView
    }
    
    func start() {
        if self.isNodeLoaded {
            self.loader.start()
        } else {
            self.onDidLoad { node in
                (node as! LoaderNode).loader.start()
            }
        }
    }
    
    func stop() {
        if self.isNodeLoaded {
            self.loader.stop()
        } else {
            self.onDidLoad { node in
                (node as! LoaderNode).loader.stop()
            }
        }
    }
    
    func success() {
        if self.isNodeLoaded {
            self.loader.successAnimation()
        } else {
            self.onDidLoad { node in
                (node as! LoaderNode).loader.successAnimation()
            }
        }
    }
    
    func errorAnimation() {
        if self.isNodeLoaded {
            self.loader.errorAnimation()
        } else {
            self.onDidLoad { node in
                (node as! LoaderNode).loader.errorAnimation()
            }
        }
    }
}

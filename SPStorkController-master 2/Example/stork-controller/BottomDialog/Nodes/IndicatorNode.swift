//
//  IndicatorNode.swift
//  stork-controller
//
//  Created by Лысков Павел on 28.02.2020.
// swiftlint:disable all
import AsyncDisplayKit

final class IndicatorNode: DLViewNode<UIActivityIndicatorView> {
    var indicator: UIActivityIndicatorView {
        return self.nodeView
    }
    
    override func didLoad() {
        super.didLoad()
        
        if #available(iOS 13.0, *) {
            indicator.style = .large
        } else {
            // Fallback on earlier versions
        }
    }
    
    func start() {
        if self.isNodeLoaded {
            self.indicator.startAnimating()
        } else {
            self.onDidLoad { node in
                (node as! IndicatorNode).indicator.startAnimating()
            }
        }
    }
    
    func stop() {
        if self.isNodeLoaded {
            self.indicator.stopAnimating()
        } else {
            self.onDidLoad { node in
                (node as! IndicatorNode).indicator.stopAnimating()
            }
        }
    }
}

//
//  TextFieldWithIconNode.swift
//  stork-controller
//
//  Created by Pavel Lyskov on 25.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import AsyncDisplayKit
import RxCocoa
import RxCocoa_Texture
import RxSwift
import TextureSwiftSupport

final class TextFieldWithIconNode: ASDisplayNode {
    struct Style {
        struct Font {
            static var text: UIFont = .systemFont(ofSize: 15)
        }
        
        struct Size {
            static var height: CGFloat = 44.0
        }
        
        struct Color {
            static var borderColor: UIColor {
                if #available(iOS 13.0, *) {
                    return UIColor.quaternaryLabel
                } else {
                    return UIColor.lightGray
                }
            }
            
            static var iconColor: UIColor {
                if #available(iOS 13.0, *) {
                    return UIColor.tertiaryLabel
                } else {
                    return UIColor.gray
                }
            }
            
            static var highlightedBorderColor: UIColor {
                if #available(iOS 13.0, *) {
                    return UIColor.secondaryLabel
                } else {
                    return UIColor.darkGray
                }
            }
        }
    }
    
    var disposeBag = DisposeBag()
    
    lazy var fieldNode = configure(PointTextFieldNode()) {
        if #available(iOS 13.0, *) {
            $0.textColor = UIColor.label
            
        } else {
            $0.textColor = UIColor.black
        }
        $0.font = Style.Font.text
    }
    
    lazy var imageNode = configure(ASImageNode()) {
        $0.contentMode = .center
    }
    
    var imageName: String
    var minCharForValid: Int = 3
    
    public lazy var isValidRelay: BehaviorRelay<Bool> = BehaviorRelay<Bool>(value: false)
    public var textDriver: BehaviorRelay<String> = BehaviorRelay<String>(value: "")
    public var isErrorRelay: BehaviorRelay<Bool> = BehaviorRelay<Bool>(value: false)
    private var enterRelay: BehaviorRelay<Bool> = BehaviorRelay<Bool>(value: false)
    
    init(imageName: String, placeholder: String, isSecurity: Bool) {
        self.imageName = imageName
        super.init()
        
        self.automaticallyManagesSubnodes = true
        if #available(iOS 13.0, *) {
            self.imageNode.image = UIImage(named: imageName)?.withTintColor(Style.Color.iconColor)
        } else {
            self.imageNode.image = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
            self.imageNode.tintColor = Style.Color.iconColor
        }
        self.cornerRadius = 8
        self.cornerRoundingType = .defaultSlowCALayer
        self.borderColor = Style.Color.borderColor.cgColor
        self.borderWidth = 1.0
        
        self.fieldNode.isSecureTextEntry = isSecurity
        self.fieldNode.nodeView.attributedPlaceholder = "\(placeholder)".at.attributed {
            $0.foreground(color: Style.Color.borderColor).font(Style.Font.text)
        }
        
        self.onDidLoad { [weak self] _ in
            guard let self = self else { return }
            
            if self.fieldNode.isNodeLoaded {
                self.bindAll()
            } else {
                self.fieldNode.onDidLoad { [weak self] _ in
                    guard let self = self else { return }
                    self.bindAll()
                }
            }
        }
    }
    
    func bindAll() {
        self.fieldNode.nodeView.rx.text.asDriver()
            .flatMapLatest { value -> Driver<String> in
                guard let text = value else {
                    return BehaviorRelay<String>(value: "").asDriver()
                }
                return BehaviorRelay<String>(value: text).asDriver()
            }.asObservable()
            .bind(to: self.textDriver)
            .disposed(by: self.disposeBag)
        self.fieldNode.nodeView.rx.text.asDriver()
            .flatMapLatest { [weak self] element -> Driver<Bool> in
                guard let self = self, let text = element else {
                    return BehaviorRelay<Bool>(value: false).asDriver()
                }
                
                let flag = text.count >= self.minCharForValid
                return BehaviorRelay<Bool>(value: flag).asDriver()
            }.asObservable()
            .bind(to: self.isValidRelay)
            .disposed(by: self.disposeBag)
        
        
        Driver.combineLatest(self.isValidRelay.asDriver(), self.isErrorRelay.asDriver(), self.enterRelay.asDriver()).flatMapLatest {[weak self] value -> Driver<Void> in
            guard let self = self else {
                return  BehaviorRelay<Void>.init(value: {}()).asDriver()
                
            }
            
            print("\(value)")
            
            if value.2 && value.1 {
                self.isErrorRelay.accept(false)
                self.regularSelectBorder()
            }
            
            if !value.2 && value.1 {
                self.setErrorBorder()
            }
            
            if !value.1 && value.0 {
                self.setValidateBorder()
            }
            
            return BehaviorRelay<Void>.init(value: {}()).asDriver()
            
            }.asDriver().drive().disposed(by: disposeBag)
        
        
//        self.isValidRelay.asDriver()
//            .drive(onNext: {valid in
//                if valid {
//                    self.setValidateBorder()
//                } else {
//                    self.setErrorBorder()
//                }
//               
//            }).disposed(by: self.disposeBag)
//        
//        self.isErrorRelay.asDriver()
//        .drive(onNext: {error in
//            guard error else {return}
//            
//            self.setErrorBorder()
//            
//        }).disposed(by: self.disposeBag)
    }
    
    func regularSelectBorder() {
        self.borderColor = Style.Color.highlightedBorderColor.cgColor
        self.setHighlightedIcon()
    }
    
    override func didLoad() {
        super.didLoad()
        
        self.fieldNode.nodeView.rx.controlEvent(.editingDidBegin)
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                 
                self.enterRelay.accept(true)
                self.regularSelectBorder()
                
            }).disposed(by: self.disposeBag)
        
        self.fieldNode.nodeView.rx.controlEvent(.editingDidEnd)
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                
                self.borderColor = Style.Color.borderColor.cgColor
                self.setRegularIcon()
                self.enterRelay.accept(false)
                
            }).disposed(by: self.disposeBag)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        return LayoutSpec {
            HStackLayout(spacing: 8.0) {
                self.imageNode.width(Style.Size.height).height(Style.Size.height)
                self.fieldNode.flexGrow(1.0)
            }
        }
    }
    
    func setRegularIcon() {
        if #available(iOS 13.0, *) {
            self.imageNode.image = UIImage(named: imageName)?.withTintColor(Style.Color.iconColor)
        } else {
            self.imageNode.image = UIImage(named: self.imageName)?.withRenderingMode(.alwaysTemplate)
            self.imageNode.tintColor = Style.Color.iconColor
        }
    }
    
    func setHighlightedIcon() {
        if #available(iOS 13.0, *) {
            self.imageNode.image = UIImage(named: self.imageName)?.withTintColor(Style.Color.highlightedBorderColor)
        } else {
            self.imageNode.image = UIImage(named: self.imageName)?.withRenderingMode(.alwaysTemplate)
            self.imageNode.tintColor = Style.Color.highlightedBorderColor
        }
    }
    
    func setValidateBorder() {
        self.borderColor = UIColor.systemBlue.cgColor
    }
    
    func setErrorBorder() {
        self.borderColor = UIColor.systemRed.cgColor
    }
}

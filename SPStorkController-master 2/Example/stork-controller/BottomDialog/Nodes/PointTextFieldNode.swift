//
//  PointTextFieldNode.swift
//  stork-controller
//
//  Created by Pavel Lyskov on 25.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import AsyncDisplayKit
// swiftlint:disable all
open class PointTextFieldNode: DLViewNode<UITextField> {
    public var isEditing: Bool {
        return self.nodeView.isEditing
    }
    
    public var isSecureTextEntry: Bool {
        get {
            return self.nodeView.isSecureTextEntry
        }
        set {
            self.appendViewAssociation { (view: UITextField) in
                view.isSecureTextEntry = newValue
            }
        }
    }
    
    public var text: String? {
        get {
            return self.nodeView.text
        }
        set {
            self.appendViewAssociation { (view: UITextField) in
                view.text = newValue
            }
        }
    }
    
    public var font: UIFont? {
        get {
            return self.nodeView.font
        }
        set {
            self.appendViewAssociation { (view: UITextField) in
                view.font = newValue
            }
        }
    }
    
    public var textColor: UIColor? {
        get {
            return self.nodeView.textColor
        }
        set {
            self.appendViewAssociation { (view: UITextField) in
                view.textColor = newValue
            }
        }
    }
    
    public var textSize: CGFloat? {
        didSet {
            let _textSize = self.textSize!
            self.appendViewAssociation { (view: UITextField) in
                view.font = UIFont.systemFont(ofSize: _textSize)
            }
        }
    }
}

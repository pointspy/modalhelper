//
//  hhh.swift
//  stork-controller
//
//  Created by Pavel Lyskov on 19.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import Lottie
import UIKit

final class LottieLoaderView: UIView {
    static var loaderEdgeSize: CGFloat = 80.0
    
    let loader = AnimationView()
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: LottieLoaderView.loaderEdgeSize, height: LottieLoaderView.loaderEdgeSize))
        
        setUpLoader()
    }
    
    private func setUpLoader() {
        if loader.animation == nil {
            if #available(iOS 13.0, *) {
            } else {
                // Fallback on earlier versions
            }
            if #available(iOS 12.0, *) {
                if self.traitCollection.userInterfaceStyle == .light {
                    loader.animation = Animation.filepath(aniPath(name: "loader3"))
                } else {
                    loader.animation = Animation.filepath(aniPath(name: "loader3_2"))
                }
            } else {
                loader.animation = Animation.filepath(aniPath(name: "loader3"))
            }
            loader.animationSpeed = 2.0
        }
        
        loader.backgroundColor = .clear
        
        addSubview(loader)
        loader.loopMode = .loop
        
        loader.frame = bounds
//        loader.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        loader.alpha = 0
        loader.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        loader.frame = bounds
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func start() {
        let timingParameters = UISpringTimingParameters(
            damping: 1.0,
            response: 0.15
        )
        
        let animator = UIViewPropertyAnimator(duration: 0, timingParameters: timingParameters)
        animator.addAnimations { [weak self] in
            guard let self = self else { return }
            self.loader.alpha = 1
            self.loader.transform = .identity
        }
        
        DispatchQueue.main.async {
            animator.startAnimation()
            self.loader.play()
        }
    }
    
    public func stop() {
        let timingParameters = UISpringTimingParameters(
            damping: 1.0,
            response: 0.15
        )
        
        let animator = UIViewPropertyAnimator(duration: 0, timingParameters: timingParameters)
        animator.addAnimations { [weak self] in
            guard let self = self else { return }
            self.loader.alpha = 0
            self.loader.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }
        
        DispatchQueue.main.async {
            animator.startAnimation()
            self.loader.stop()
        }
    }
    
    private func aniPath(name: String) -> String {
        let lottieBundlePath = Bundle.main.path(forResource: "lottie", ofType: "bundle") ?? ""
        let lottieBundle = Bundle(path: lottieBundlePath)
        let aniPath = lottieBundle?.path(forResource: name, ofType: "json") ?? ""
        
        return aniPath
    }
    
    public func setAnimation(with name: String) {
        loader.animation = Animation.filepath(aniPath(name: name))
    }
    
    public func successAnimation() {
        loader.animationSpeed = 0.33
        setAnimation(with: "successMy")
        
        DispatchQueue.main.async {
            self.start()
        }
    }
    
    public func errorAnimation() {
        loader.animationSpeed = 0.7
        setAnimation(with: "connection-error")
        
        DispatchQueue.main.async {
            self.start()
        }
    }
}

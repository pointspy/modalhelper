//
//  CAMediaTimeFunctiomExtension.swift
//  Guideo
//
//  Created by Умная Логистика on 01/07/2019.
//  Copyright © 2019 Умная Логистика. All rights reserved.
//

import UIKit

extension CAMediaTimingFunction {
    
    // standards
    @nonobjc public static let linear = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
    @nonobjc public static let easeIn = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
    @nonobjc public static let easeOut = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
    @nonobjc public static let easeInOut = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    @nonobjc public static let `default`  = CAMediaTimingFunction(name: CAMediaTimingFunctionName.default)
    
    // http://easings.net/
    @nonobjc public static let easeInSine = CAMediaTimingFunction(controlPoints: 0.47, 0, 0.745, 0.715)
    @nonobjc public static let easeOutSine = CAMediaTimingFunction(controlPoints: 0.39, 0.575, 0.565, 1)
    @nonobjc public static let easeInOutSine = CAMediaTimingFunction(controlPoints: 0.445, 0.05, 0.55, 0.95)
    
    @nonobjc public static let easeInQuad = CAMediaTimingFunction(controlPoints: 0.55, 0.085, 0.68, 0.53)
    @nonobjc public static let easeOutQuad = CAMediaTimingFunction(controlPoints: 0.25, 0.46, 0.45, 0.94)
    @nonobjc public static let easeInOutQuad = CAMediaTimingFunction(controlPoints: 0.455, 0.03, 0.515, 0.955)
    
    @nonobjc public static let easeInCubic = CAMediaTimingFunction(controlPoints: 0.55, 0.055, 0.675, 0.19)
    @nonobjc public static let easeOutCubic = CAMediaTimingFunction(controlPoints: 0.215, 0.61, 0.355, 1)
    @nonobjc public static let easeInOutCubic = CAMediaTimingFunction(controlPoints: 0.645, 0.045, 0.355, 1)
    
    @nonobjc public static let easeInQuart = CAMediaTimingFunction(controlPoints: 0.895, 0.03, 0.685, 0.22)
    @nonobjc public static let easeOutQuart = CAMediaTimingFunction(controlPoints: 0.165, 0.84, 0.44, 1)
    @nonobjc public static let easeInOutQuart = CAMediaTimingFunction(controlPoints: 0.77, 0, 0.175, 1)
    
    @nonobjc public static let easeInQuint = CAMediaTimingFunction(controlPoints: 0.755, 0.05, 0.855, 0.06)
    @nonobjc public static let easeOutQuint = CAMediaTimingFunction(controlPoints: 0.23, 1, 0.32, 1)
    @nonobjc public static let easeInOutQuint = CAMediaTimingFunction(controlPoints: 0.86, 0, 0.07, 1)
    
    @nonobjc public static let easeInExpo = CAMediaTimingFunction(controlPoints: 0.95, 0.05, 0.795, 0.035)
    @nonobjc public static let easeOutExpo = CAMediaTimingFunction(controlPoints: 0.19, 1, 0.22, 1)
    @nonobjc public static let easeInOutExpo = CAMediaTimingFunction(controlPoints: 1, 0, 0, 1)
    
    @nonobjc public static let easeInCirc = CAMediaTimingFunction(controlPoints: 0.6, 0.04, 0.98, 0.335)
    @nonobjc public static let easeOutCirc = CAMediaTimingFunction(controlPoints: 0.075, 0.82, 0.165, 1)
    @nonobjc public static let easeInOutCirc = CAMediaTimingFunction(controlPoints: 0.785, 0.135, 0.15, 0.86)
    
    @nonobjc public static let easeInBack = CAMediaTimingFunction(controlPoints: 0.6, -0.28, 0.735, 0.045)
    @nonobjc public static let easeOutBack = CAMediaTimingFunction(controlPoints: 0.175, 0.885, 0.32, 1.275)
    @nonobjc public static let easeInOutBack = CAMediaTimingFunction(controlPoints: 0.68, -0.55, 0.265, 1.55)
    
    @nonobjc public static let easeBothInvision = CAMediaTimingFunction(controlPoints:  0.64, 0.04, 0.35, 1)
    @nonobjc public static let easeOutInvision = CAMediaTimingFunction(controlPoints:  0.22, 0.61, 0.35, 1)
    @nonobjc public static let easeInInvision = CAMediaTimingFunction(controlPoints:  0.55, 0.05, 0.67, 0.19)
    @nonobjc public static let myInOut = CAMediaTimingFunction(controlPoints:  0.64, 0.04, 0.12, 1)
    @nonobjc public static let principleCurve = CAMediaTimingFunction(controlPoints:  0.250, 0.100, 0.250, 1)
    @nonobjc public static let easeInOut12UX = CAMediaTimingFunction(controlPoints:  0.500, 0.100, 0.120, 1)
    @nonobjc public static let flowIn = CAMediaTimingFunction(controlPoints:  0.47, 0.000, 0.745, 0.715)
    @nonobjc public static let uxInMotionCurve = CAMediaTimingFunction(controlPoints:  0.130, 0.000, 0.110, 1.000)
    @nonobjc public static let uxOut = CAMediaTimingFunction(controlPoints:  0.0, 0.000, 0.110, 1.000)
}
    
    


extension CAMediaTimingFunction {
    
    /// Return the control points of the timing function.
    public var controlPoints: ((x: Float, y: Float), (x: Float, y: Float)) {
        var cps = [Float](repeating: 0, count: 4)
        getControlPoint(at: 0, values: &cps[0])
        getControlPoint(at: 1, values: &cps[1])
        getControlPoint(at: 2, values: &cps[2])
        getControlPoint(at: 3, values: &cps[3])
        return ((cps[0], cps[1]), (cps[2], cps[3]))
    }
    
}

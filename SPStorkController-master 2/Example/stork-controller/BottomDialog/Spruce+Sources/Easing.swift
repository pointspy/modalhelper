

import Foundation
import CoreGraphics
// swiftlint:disable all
public protocol DoubleConvertable {
    init(_ v: Double)
    func asDouble() -> Double;
}

extension Double: DoubleConvertable {
    public func asDouble() -> Double {
        return self
    }
}

extension Float: DoubleConvertable {
    public func asDouble() -> Double {
        return Double(self)
    }
}

extension CGFloat: DoubleConvertable {
    public func asDouble() -> Double {
        return Double(self)
    }
}

public enum Easing
{
    case linear
    case linearUnlimited
    
    case smoothStep
    case smootherStep
    
    case quadraticEaseIn
    case quadraticEaseOut
    case quadraticEaseInOut
    
    case cubicEaseIn
    case cubicEaseOut
    case cubicEaseInOut
    
    case quarticEaseIn
    case quarticEaseOut
    case quarticEaseInOut
    
    case quinticEaseIn
    case quinticEaseOut
    case quinticEaseInOut
    
    case sineEaseIn
    case sineEaseOut
    case sineEaseInOut
    
    case circularEaseIn
    case circularEaseOut
    case circularEaseInOut
    
    case exponentialEaseIn
    case exponentialEaseOut
    case exponentialEaseInOut
    
    case elasticEaseIn
    case elasticEaseOut
    case elasticEaseInOut
    
    case backEaseIn
    case backEaseOut
    case backEaseInOut
    
    case bounceEaseIn
    case bounceEaseOut
    case bounceEaseInOut
    
    case cubicBezier(Double, Double, Double, Double)
    
    case springCuberto(Double, Double, Double)
    
    case easeBothInvision
    
    public func getValuesBetween(start: Double, finish: Double, t1: Double, t2: Double, for times: Int) -> ([Double], [Double])
    {
        let timeKeys = Array(stride(from: t1, through: t2, by: (t2 - t1) / Double(times)))
        
        var values: [Double] = []
        
        timeKeys.forEach
        { t in
            values.append(self._calculate(g1: t1, d1: start, g2: t2, d2: finish, g: t, withEase: true))
        }
        
        return (values, timeKeys)
    }
    
    /**
     Calculate value g where g1 = 0, d1 = 0, g2 = 1, d2 = 1
     */
    public func calculate<T: DoubleConvertable>(_ g: T) -> T
    {
        return T(_calculate(g1: 0, d1: 0, g2: 1, d2: 1, g: g.asDouble(), withEase: true))
    }
    
    /**
     Calculate value g where g1 = 0, d1 = d1, g2 = 1, d2 = d2
     */
    public func calculate<T: DoubleConvertable>(d1: T, d2: T, g: T, withEase: Bool) -> T
    {
        return T(_calculate(g1: 0, d1: d1.asDouble(), g2: 1, d2: d2.asDouble(), g: g.asDouble(), withEase: withEase))
    }
    
    public func calculate(d1: SIMD2<Double>, d2: SIMD2<Double>, g: Double) -> SIMD2<Double>
    {
        let resT: Double = easingFunction(g)
        
        return (d1 + resT * (d2 - d1))
    }
    
    /**
     Calculate value g where g1 = g1, d1 = d1, g2 = g2, d2 = d2
     */
    public func calculate<T: DoubleConvertable>(g1: T, d1: T, g2: T, d2: T, g: T) -> T
    {
        return T(_calculate(g1: g1.asDouble(), d1: d1.asDouble(), g2: g2.asDouble(), d2: d2.asDouble(), g: g.asDouble(), withEase: true))
    }
    
    public func calculateValueForProgressWithoutEase<T: DoubleConvertable>(g1: T, d1: T, g2: T, d2: T, g: T) -> T
    {
        return T(_calculate(g1: g1.asDouble(), d1: d1.asDouble(), g2: g2.asDouble(), d2: d2.asDouble(), g: g.asDouble(), withEase: false))
    }
    
    private func _calculate(g1: Double, d1: Double, g2: Double, d2: Double, g: Double, withEase: Bool) -> Double
    {
        var g = g
        
        switch self
        {
        case .linearUnlimited:
            break
        default:
            g = max(min(g1, g2), g)
            g = min(max(g1, g2), g)
        }
        
        let t = (g - g1) / (g2 - g1)

        var resT: Double
        if withEase
        {
            resT = easingFunction(t)
        }
        else
        {
            resT = t
        }
        return (d1 + resT * (d2 - d1))
    }
    
    private var easingFunction: (Double) -> Double
    {
        switch self
        {
        case .linear:
            return Easing._linear
        case .linearUnlimited:
            return Easing._linear
        case .smoothStep:
            return Easing._smoothStep
        case .smootherStep:
            return Easing._smootherStep
        case .quadraticEaseIn:
            return Easing._quarticEaseIn
        case .quadraticEaseOut:
            return Easing._quadraticEaseOut
        case .quadraticEaseInOut:
            return Easing._quadraticEaseInOut
        case .cubicEaseIn:
            return Easing._cubicEaseIn
        case .cubicEaseOut:
            return Easing._cubicEaseOut
        case .cubicEaseInOut:
            return Easing._cubicEaseInOut
        case .quarticEaseIn:
            return Easing._quarticEaseIn
        case .quarticEaseOut:
            return Easing._quarticEaseOut
        case .quarticEaseInOut:
            return Easing._quarticEaseInOut
        case .quinticEaseIn:
            return Easing._quinticEaseIn
        case .quinticEaseOut:
            return Easing._quinticEaseOut
        case .quinticEaseInOut:
            return Easing._quinticEaseInOut
        case .sineEaseIn:
            return Easing._sineEaseIn
        case .sineEaseOut:
            return Easing._sineEaseOut
        case .sineEaseInOut:
            return Easing._sineEaseInOut
        case .circularEaseIn:
            return Easing._circularEaseIn
        case .circularEaseOut:
            return Easing._circularEaseOut
        case .circularEaseInOut:
            return Easing._circularEaseInOut
        case .exponentialEaseIn:
            return Easing._exponentialEaseIn
        case .exponentialEaseOut:
            return Easing._exponentialEaseOut
        case .exponentialEaseInOut:
            return Easing._exponentialEaseInOut
        case .elasticEaseIn:
            return Easing._elasticEaseIn
        case .elasticEaseOut:
            return Easing._elasticEaseOut
        case .elasticEaseInOut:
            return Easing._elasticEaseInOut
        case .backEaseIn:
            return Easing._backEaseIn
        case .backEaseOut:
            return Easing._backEaseOut
        case .backEaseInOut:
            return Easing._backEaseInOut
        case .bounceEaseIn:
            return Easing._bounceEaseIn
        case .bounceEaseOut:
            return Easing._bounceEaseOut
        case .bounceEaseInOut:
            return Easing._bounceEaseInOut
        case .cubicBezier(let x1, let y1, let x2, let y2):
            return
                { (p: Double) -> Double in
                    Easing._cubicBezier(p, x1: x1, y1: y1, x2: x2, y2: y2)
                }
        case .easeBothInvision:
            return
                { (p: Double) -> Double in
                    Easing._cubicBezier(p, x1: 0.64, y1: 0.04, x2: 0.35, y2: 1.0)
                }
        case .springCuberto(let time, let startOffset, let damping):
            return
                { (_: Double) -> Double in
                    Easing._springCuberto(time: time, startOffset: startOffset, damping: damping)
                }
        }
    }
    
    private static func _springCuberto(time: Double, startOffset: Double, damping: Double) -> Double
    {
        let A: Double = startOffset
        let r: Double = 40
        let m: Double = 6
        let beta: Double = r / (2 * m)
        let k: Double = 20 + 100 * damping
        let omega0: Double = k / m
        let omega: Double = pow(-pow(beta, 2) + pow(omega0, 2), 0.5)
        
        return A * exp(-beta * time) * cos(omega * time) / 100
    }
    
    // Modeled after the line y = x
    private static func _linear(_ p: Double) -> Double
    {
        return p
    }
    
    // fast but ugly easeInOut
    private static func _smoothStep(_ p: Double) -> Double
    {
        return p * p * (3 - 2 * p)
    }
    
    // fast but ugly easeInOut
    private static func _smootherStep(_ p: Double) -> Double
    {
        return p * p * p * (p * (p * 6 - 15) + 10)
    }
    
    // Modeled after the parabola y = x^2
    private static func _quadraticEaseIn(_ p: Double) -> Double
    {
        return p * p
    }
    
    // Modeled after the parabola y = -x^2 + 2x
    private static func _quadraticEaseOut(_ p: Double) -> Double
    {
        return -(p * (p - 2))
    }
    
    // Modeled after the piecewise quadratic
    // y = (1/2)((2x)^2)             ; [0, 0.5)
    // y = -(1/2)((2x-1)*(2x-3) - 1) ; [0.5, 1]
    private static func _quadraticEaseInOut(_ p: Double) -> Double
    {
        if p < 0.5
        {
            return 2 * p * p
        }
        else
        {
            return (-2 * p * p) + (4 * p) - 1
        }
    }
    
    // Modeled after the cubic y = x^3
    private static func _cubicEaseIn(_ p: Double) -> Double
    {
        return p * p * p
    }
    
    // Modeled after the cubic y = (x - 1)^3 + 1
    private static func _cubicEaseOut(_ p: Double) -> Double
    {
        let f = (p - 1)
        return f * f * f + 1
    }
    
    // Modeled after the piecewise cubic
    // y = (1/2)((2x)^3)       ; [0, 0.5)
    // y = (1/2)((2x-2)^3 + 2) ; [0.5, 1]
    private static func _cubicEaseInOut(_ p: Double) -> Double
    {
        if p < 0.5
        {
            return 4 * p * p * p
        }
        else
        {
            let f = ((2 * p) - 2)
            return 0.5 * f * f * f + 1
        }
    }
    
    // Modeled after the quartic x^4
    private static func _quarticEaseIn(_ p: Double) -> Double
    {
        return p * p * p * p
    }
    
    // Modeled after the quartic y = 1 - (x - 1)^4
    private static func _quarticEaseOut(_ p: Double) -> Double
    {
        let f = (p - 1)
        return f * f * f * (1 - p) + 1
    }
    
    // Modeled after the piecewise quartic
    // y = (1/2)((2x)^4)        ; [0, 0.5)
    // y = -(1/2)((2x-2)^4 - 2) ; [0.5, 1]
    private static func _quarticEaseInOut(_ p: Double) -> Double
    {
        if p < 0.5
        {
            return 8 * p * p * p * p
        }
        else
        {
            let f = (p - 1)
            return -8 * f * f * f * f + 1
        }
    }
    
    // Modeled after the quintic y = x^5
    private static func _quinticEaseIn(_ p: Double) -> Double
    {
        return p * p * p * p * p
    }
    
    // Modeled after the quintic y = (x - 1)^5 + 1
    private static func _quinticEaseOut(_ p: Double) -> Double
    {
        let f = (p - 1)
        return f * f * f * f * f + 1
    }
    
    // Modeled after the piecewise quintic
    // y = (1/2)((2x)^5)       ; [0, 0.5)
    // y = (1/2)((2x-2)^5 + 2) ; [0.5, 1]
    private static func _quinticEaseInOut(_ p: Double) -> Double
    {
        if p < 0.5
        {
            return 16 * p * p * p * p * p
        }
        else
        {
            let f = ((2 * p) - 2)
            return 0.5 * f * f * f * f * f + 1
        }
    }
    
    // Modeled after quarter-cycle of sine wave
    private static func _sineEaseIn(_ p: Double) -> Double
    {
        return sin((p - 1) * .pi / 2) + 1
    }
    
    // Modeled after quarter-cycle of sine wave (different phase)
    private static func _sineEaseOut(_ p: Double) -> Double
    {
        return sin(p * .pi / 2)
    }
    
    // Modeled after half sine wave
    private static func _sineEaseInOut(_ p: Double) -> Double
    {
        return 0.5 * (1 - cos(p * .pi))
    }
    
    // Modeled after shifted quadrant IV of unit circle
    private static func _circularEaseIn(_ p: Double) -> Double
    {
        return 1 - sqrt(1 - (p * p))
    }
    
    // Modeled after shifted quadrant II of unit circle
    private static func _circularEaseOut(_ p: Double) -> Double
    {
        return sqrt((2 - p) * p)
    }
    
    // Modeled after the piecewise circular function
    // y = (1/2)(1 - sqrt(1 - 4x^2))           ; [0, 0.5)
    // y = (1/2)(sqrt(-(2x - 3)*(2x - 1)) + 1) ; [0.5, 1]
    private static func _circularEaseInOut(_ p: Double) -> Double
    {
        if p < 0.5
        {
            return 0.5 * (1 - sqrt(1 - 4 * (p * p)))
        }
        else
        {
            return 0.5 * (sqrt(-((2 * p) - 3) * ((2 * p) - 1)) + 1)
        }
    }
    
    // Modeled after the exponential function y = 2^(10(x - 1))
    private static func _exponentialEaseIn(_ p: Double) -> Double
    {
        return (p == 0.0) ? p : pow(2, 10 * (p - 1))
    }
    
    // Modeled after the exponential function y = -2^(-10x) + 1
    private static func _exponentialEaseOut(_ p: Double) -> Double
    {
        return (p == 1.0) ? p : 1 - pow(2, -10 * p)
    }
    
    // Modeled after the piecewise exponential
    // y = (1/2)2^(10(2x - 1))         ; [0,0.5)
    // y = -(1/2)*2^(-10(2x - 1))) + 1 ; [0.5,1]
    private static func _exponentialEaseInOut(_ p: Double) -> Double
    {
        if p == 0.0 || p == 1.0
        {
            return p
        }
        
        if p < 0.5
        {
            return 0.5 * pow(2, (20 * p) - 10)
        }
        else
        {
            return -0.5 * pow(2, (-20 * p) + 10) + 1
        }
    }
    
    // Modeled after the damped sine wave y = sin(13pi/2*x)*pow(2, 10 * (x - 1))
    private static func _elasticEaseIn(_ p: Double) -> Double
    {
        return sin(13 * .pi / 2 * p) * pow(2, 10 * (p - 1))
    }
    
    // Modeled after the damped sine wave y = sin(-13pi/2*(x + 1))*pow(2, -10x) + 1
    private static func _elasticEaseOut(_ p: Double) -> Double
    {
        return sin(-13 * .pi / 2 * (p + 1)) * pow(2, -10 * p) + 1
    }
    
    // Modeled after the piecewise exponentially-damped sine wave:
    // y = (1/2)*sin(13pi/2*(2*x))*pow(2, 10 * ((2*x) - 1))      ; [0,0.5)
    // y = (1/2)*(sin(-13pi/2*((2x-1)+1))*pow(2,-10(2*x-1)) + 2) ; [0.5, 1]
    private static func _elasticEaseInOut(_ p: Double) -> Double
    {
        if p < 0.5
        {
            return 0.5 * sin(13 * .pi / 2 * (2 * p)) * pow(2, 10 * ((2 * p) - 1))
        }
        else
        {
            return 0.5 * (sin(-13 * .pi / 2 * ((2 * p - 1) + 1)) * pow(2, -10 * (2 * p - 1)) + 2)
        }
    }
    
    // Modeled after the overshooting cubic y = x^3-x*sin(x*pi)
    private static func _backEaseIn(_ p: Double) -> Double
    {
        return p * p * p - p * sin(p * .pi)
    }
    
    // Modeled after overshooting cubic y = 1-((1-x)^3-(1-x)*sin((1-x)*pi))
    private static func _backEaseOut(_ p: Double) -> Double
    {
        let f = (1 - p)
        return 1 - (f * f * f - f * sin(f * .pi))
    }
    
    // Modeled after the piecewise overshooting cubic function:
    // y = (1/2)*((2x)^3-(2x)*sin(2*x*pi))           ; [0, 0.5)
    // y = (1/2)*(1-((1-x)^3-(1-x)*sin((1-x)*pi))+1) ; [0.5, 1]
    private static func _backEaseInOut(_ p: Double) -> Double
    {
        if p < 0.5
        {
            let f = 2 * p
            let x = (f * f * f - f * sin(f * .pi))
            return 0.5 * x
        }
        else
        {
            let f = (1 - (2 * p - 1))
            let x = (f * f * f - f * sin(f * .pi))
            return 0.5 * (1 - x) + 0.5
        }
    }
    
    private static func _bounceEaseIn(_ p: Double) -> Double
    {
        return 1 - _bounceEaseOut(1 - p)
    }
    
    private static func _bounceEaseOut(_ p: Double) -> Double
    {
        if p < 4 / 11.0
        {
            return (121 * p * p) / 16.0
        }
        else if p < 8 / 11.0
        {
            return (363 / 40.0 * p * p) - (99 / 10.0 * p) + 17 / 5.0
        }
        else if p < 9 / 10.0
        {
            return (4356 / 361.0 * p * p) - (35442 / 1805.0 * p) + 16061 / 1805.0
        }
        else
        {
            return (54 / 5.0 * p * p) - (513 / 25.0 * p) + 268 / 25.0
        }
    }
    
    private static func _bounceEaseInOut(_ p: Double) -> Double
    {
        if p < 0.5
        {
            return 0.5 * _bounceEaseIn(p * 2)
        }
        else
        {
            return 0.5 * _bounceEaseOut(p * 2 - 1) + 0.5
        }
    }
    
    private static func _cubicBezier(_ p: Double, x1: Double, y1: Double, x2: Double, y2: Double) -> Double
    {
        return CubicBezierCalculator(x1: x1, y1: y1, x2: x2, y2: y2).calculate(p)
    }
}

struct CubicBezierCalculator {
    
    let x1: Double
    let y1: Double
    let x2: Double
    let y2: Double
    
    private let sampleValues: [Double]
    
    init(x1: Double, y1: Double, x2: Double, y2: Double, samplesCount: Int = 10) {
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        
        if (x1 == y1 && x2 == y2) {
            sampleValues = []
        } else {
            sampleValues = CubicBezierCalculator.calculateSampleValues(x1, x2, samplesCount)
        }
        
    }
    
    func calculate(_ x: Double) -> Double {
        if (x1 == y1 && x2 == y2) {
            return x
        }
        return CubicBezierCalculator.calculateBezier(calculateT(x), y1, y2);
    }
    
    private func calculateT(_ x: Double) -> Double {
        
        
        let stepSize: Double = 1.0 / Double(sampleValues.count)
        
        // Find interval where t lies
        var intervalStart = 0.0
        
        var currentSample = 0
        
        for (idx, value) in sampleValues.enumerated() {
            
            if idx == sampleValues.count - 1 {
                break
            }
            
            if value >= x {
                break
            }
            
            currentSample = idx
            if idx > 0 {
                intervalStart += stepSize
            }
        }
        
        // Interpolate to provide an initial guess for t
        let startGuess = sampleValues[currentSample]
        let endGuess = sampleValues[currentSample + 1]
        let dist = (x - startGuess)/(endGuess - startGuess)
        
        let guessForT = intervalStart + dist * stepSize
        
        // Check the slope to see what strategy to use. If the slope is too small
        // Newton-Raphson iteration won't converge on a root so we use bisection
        // instead.
        
        let initialSlope = CubicBezierCalculator.getSlope(guessForT, x1, x2);
        if initialSlope >= 0.02 {
            return CubicBezierCalculator.newtonRaphsonIterate(x, guessForT, x1, x2);
        } else if initialSlope == 0 {
            return guessForT;
        } else {
            return CubicBezierCalculator.binarySubdivide(x, intervalStart, intervalStart + stepSize, x1, x2);
        }
    }
    
    private static func calculateSampleValues(_ x1: Double, _ x2: Double, _ samplesCount: Int) -> [Double] {
        
        struct Cache {
            static var dict: [SampleKey: [Double]] = [:]
        }
        
        let key = SampleKey(x1: x1, x2: x2, number: samplesCount)
        
        if let cached = Cache.dict[key] {
            return cached
        }
        
        let stepSize: Double = 1.0 / Double(samplesCount)
        
        var sampleValues: [Double] = []
        for i in 0...samplesCount {
            
            sampleValues.append(calculateBezier(Double(i) * stepSize, x1, x2))
        }
        
        Cache.dict[key] = sampleValues
        
        return sampleValues
    }
    
    
    private static func calculateBezier(_ t: Double, _ a1: Double, _ a2: Double) -> Double {
        // use Horner's scheme to evaluate the Bezier polynomial
        return ((calculateA(a1, a2)*t + calculateB(a1, a2))*t + calculateC(a1))*t;
    }
    
    
    private static func calculateA(_ a1: Double, _ a2: Double) -> Double {
        return 1.0 - 3.0 * a2 + 3.0 * a1
    }
    
    private static func calculateB(_ a1: Double, _ a2: Double) -> Double {
        return 3.0 * a2 - 6.0 * a1
    }
    
    private static func calculateC(_ a1: Double) -> Double {
        return 3.0 * a1
    }
    
    private static func getSlope(_ aT: Double, _ a1: Double, _ a2: Double) -> Double {
        return 3.0 * calculateA(a1, a2)*aT*aT + 2.0 * calculateB(a1, a2) * aT + calculateC(a1);
    }
    
    private static func newtonRaphsonIterate(_ x: Double, _ guessT: Double, _ x1: Double, _ x2: Double) -> Double
    {
        var guessT = guessT
        // Refine guess with Newton-Raphson iteration
        for _ in 0..<4 {
            // We're trying to find where f(t) = x,
            // so we're actually looking for a root for: CalcBezier(t) - x
            let currentX = calculateBezier(guessT, x1, x2) - x;
            let currentSlope = getSlope(guessT, x1, x2);
            if (currentSlope == 0.0) {
                return guessT;
            }
            guessT -= currentX / currentSlope;
        }
        return guessT;
    }
    
    private static func binarySubdivide(_ x: Double, _ a: Double, _ b: Double, _ x1: Double, _ x2: Double) -> Double
    {
        var a = a
        var b = b
        var currentX: Double = 0
        var currentT: Double = 0
        var i: Int = 0
        repeat {
            currentT = a + (b - a) / 2.0;
            currentX = calculateBezier(currentT, x1, x2) - x;
            if currentX > 0.0 {
                b = currentT;
            } else {
                a = currentT;
            }
            i += 1
        } while (fabs(currentX) > 0.0000001)
            && (i < 11);
        
        return currentT;
    }
    
}

private struct SampleKey: Hashable {
    let x1: Double
    let x2: Double
    let number: Int
    
    var hashValue: Int {
        return (31 &* x1.hashValue) &+ x2.hashValue
    }
}

private func ==(left: SampleKey, right: SampleKey) -> Bool {
    return (left.x1 == right.x1) && (left.x2 == right.x2) && (left.number == right.number)
}

extension Easing {
    
    
    public func calculateOptimizedTime(for translation: CGFloat, minDuration: TimeInterval, maxDuration: TimeInterval, minTranslation: CGFloat, maxTranslation: CGFloat) -> TimeInterval {
        
        
        return self._calculate(g1: minTranslation.asDouble(), d1: minDuration, g2: maxTranslation.asDouble(), d2: maxDuration, g: translation.asDouble(), withEase: true)
        
    }
    
    
}

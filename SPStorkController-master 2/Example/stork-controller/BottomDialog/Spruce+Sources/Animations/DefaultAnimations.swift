//
//  DefaultAnimations.swift
//  Spruce
//
//  Copyright (c) 2017 WillowTree, Inc.

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import UIKit

extension Spruce {
    /// Spruce.Direction that a slide animation should use.
    ///
    /// - up: start the view below its current position, and then slide upwards to where it currently is
    /// - down: start the view above its current position, and then slide downwards to where it currently is
    /// - left: start the view to the right of its current position, and then slide left to where it currently is
    /// - right: start the view to the left of its current position, and then slide right to where it currently is
    public enum SlideDirection {
        /// start the view below its current position, and then slide upwards to where it currently is
        case up
        
        /// start the view above its current position, and then slide downwards to where it currently is
        case down
        
        /// start the view to the right of its current position, and then slide left to where it currently is
        case left
        
        /// start the view to the left of its current position, and then slide right to where it currently is
        case right
    }
    
    /// How much the angle of an animation should change. This value changes based off of which type of `StockAnimation` is used.
    ///
    /// - weakly : weakly  animate the object
    /// - medium : the object should move a moderate amount
    /// - strong : the object should move very noticeably
    /// - toAngle: provide your own angle value that you feel the object should rotate
    public enum Angle {
        /// weakly  animate the object
        case weakly
        
        /// the object should move a moderate amount
        case medium
        
        /// the object should move very noticeably
        case strong
        
        /// provide your own value that you feel the object should move. The value you should provide should be a `Double`
        case toAngle(CGFloat)
    }
    
    /// How much the scale of an animation should change. This value changes based off of which type of `StockAnimation` is used.
    ///
    /// - weakly : weakly  animate the object
    /// - medium : the object should scale a moderate amount
    /// - strong : the object should scale very noticeably
    /// - toScale: provide your own scale value that you feel the object should grow/shrink
    public enum Scale {
        /// weakly  animate the object
        case weakly
        
        /// the object should scale a moderate amount
        case medium
        
        /// the object should scale very noticeably
        case strong
        
        /// provide your own scale value that you feel the object should grow/shrink
        case toScale(CGFloat)
    }
    
    /// How much the distance of a view animation should change. This value changes based off of which type of `StockAnimation` is used.
    ///
    /// - weakly : weakly  move the object
    /// - medium : the object should move a moderate amount
    /// - strong : the object should move very noticeably
    /// - byPoints: provide your own distance value that you feel the object should slide over
    public enum Distance {
        /// weakly  move the object
        case weakly
        
        /// the object should move a moderate amount
        case medium
        
        /// the object should move very noticeably
        case strong
        
        /// provide your own distance value that you feel the object should slide over
        case byPoints(CGFloat)
    }
    
    /// A few stock animations that you can use with Spruce. We want to make it really easy for you to include animations so we tried to include the basics. Use these stock animations to `slide`, `fade`, `spin`, `expand`, or `contract` your views.
    public enum StockAnimation {
        /// Have your view slide to where it currently is. Provide a `SlideDirection` and `Size` to determine what the animation should look like.
        case slide(SlideDirection, Distance)
        
        /// Fade the view in
        case fadeIn
        
        /// Spin the view in the direction of the size. Provide a `Size` to define how much the view should spin
        case spin(Angle)
        
        /// Have the view grow in size. Provide a `Size` to define by which scale the view should grow
        case expand(Scale)
        
        /// Have the view shrink in size. Provide a `Size` to define by which scale the view should shrink
        case contract(Scale)
        
        /// Provide custom prepare and change functions for the view to animate
        case custom(prepareFunction: PrepareHandler, animateFunction: ChangeFunction)
        
        /// Given the `StockAnimation`, how should Spruce prepare your view for animation. Since we want all of the views to end the animation where they are supposed to be positioned, we have to reverse their animation effect.
        var prepareFunction: PrepareHandler {
            switch self {
            case .slide:
                let offset = slideOffset
                return { view in
                    let currentTransform = view.transform
                    let offsetTransform = CGAffineTransform(translationX: offset.x, y: offset.y)
                    view.transform = currentTransform.concatenating(offsetTransform)
                }
            case .fadeIn:
                return { view in
                    view.alpha = 0.0
                }
            case .spin:
                let angle = spinAngle
                return { view in
                    let currentTransform = view.transform
                    let spinTransform = CGAffineTransform(rotationAngle: angle)
                    view.transform = currentTransform.concatenating(spinTransform)
                }
            case .expand, .contract:
                let scale = self.scale
                return { view in
                    let currentTransform = view.transform
                    let scaleTransform = CGAffineTransform(scaleX: scale, y: scale)
                    view.transform = currentTransform.concatenating(scaleTransform)
                }
            case .custom(let prepare, _):
                return prepare
            }
        }
        
        /// Reset any of the transforms on the view so that the view will end up in its original position. If a `custom` animation is used, then that animation `ChangeFunction` is returned.
        var animationFunction: ChangeFunction {
            switch self {
            case .slide:
                return { view in
                    view.transform = CGAffineTransform(translationX: 0.0, y: 0.0)
                }
            case .fadeIn:
                return { view in
                    view.alpha = 1.0
                }
            case .spin:
                return { view in
                    view.transform = CGAffineTransform(rotationAngle: 0.0)
                }
            case .expand, .contract:
                return { view in
                    view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }
            case .custom(_, let animation):
                return animation
            }
        }
        
        /// Given the animation is a `slide`, return the slide offset
        var slideOffset: CGPoint {
            switch self {
            case .slide(let direction, let size):
                switch (direction, size) {
                case (.up, .weakly ):
                    return CGPoint(x: 0.0, y: 13.0)
                case (.up, .medium ):
                    return CGPoint(x: 0.0, y: 34.0)
                case (.up, .strong ):
                    return CGPoint(x: 0.0, y: 55.0)
                case (.up, .byPoints(let value)):
                    return CGPoint(x: 0.0, y: -value)
                case (.down, .weakly ):
                    return CGPoint(x: 0.0, y: -13.0)
                case (.down, .medium ):
                    return CGPoint(x: 0.0, y: -34.0)
                case (.down, .strong ):
                    return CGPoint(x: 0.0, y: -55.0)
                case (.down, .byPoints(let value)):
                    return CGPoint(x: 0.0, y: -value)
                case (.left, .weakly ):
                    return CGPoint(x: 13.0, y: 0.0)
                case (.left, .medium ):
                    return CGPoint(x: 34.0, y: 0.0)
                case (.left, .strong ):
                    return CGPoint(x: 55.0, y: 0.0)
                case (.left, .byPoints(let value)):
                    return CGPoint(x: -value, y: 0.0)
                case (.right, .weakly ):
                    return CGPoint(x: -13.0, y: 0.0)
                case (.right, .medium ):
                    return CGPoint(x: -34.0, y: 0.0)
                case (.right, .strong ):
                    return CGPoint(x: -55.0, y: 0.0)
                case (.right, .byPoints(let value)):
                    return CGPoint(x: -value, y: 0.0)
                }
            default:
                return CGPoint.zero
            }
        }
        
        /// Given the animation is a `spin`, then this will return the angle that the view should spin.
        var spinAngle: CGFloat {
            switch self {
            case .spin(let size):
                switch size {
                case .weakly :
                    return CGFloat(Double.pi / 4)
                case .medium :
                    return CGFloat(Double.pi / 2)
                case .strong :
                    return CGFloat(Double.pi)
                case .toAngle(let value):
                    return value
                }
            default:
                return 0.0
            }
        }
        
        /// Given the animation is either `expand` or `contract`, this will return the scale from which the view should grow/shrink
        var scale: CGFloat {
            switch self {
            case .contract(let size):
                switch size {
                case .weakly :
                    return 1.1
                case .medium :
                    return 1.3
                case .strong :
                    return 1.5
                case .toScale(let value):
                    return value
                }
            case .expand(let size):
                switch size {
                case .weakly :
                    return 0.9
                case .medium :
                    return 0.7
                case .strong :
                    return 0.5
                case .toScale(let value):
                    return value
                }
            default:
                return 0.0
            }
        }
    }
}

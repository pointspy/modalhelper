//
//  BottomSheetViewModelImpl.swift
//  stork-controller
//
//  Created by Лысков Павел on 06.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import Action
import RxCocoa
import RxSwift
import XCoordinator

class BottomSheetViewModelImpl: BottomSheetViewModel, BottomSheetViewModelInput, BottomSheetViewModelOutput {
   
    
    // MARK: Inputs

    private(set) lazy var nextTrigger = nextAction.inputs
    private(set) lazy var closeTrigger = closeAction.inputs

    // MARK: Actions

    var oldState: BottomSheetState = .initialState

    private lazy var nextAction: Action<BottomSheetState, BottomSheetState> = Action { [weak self] state in
        guard let self = self else {
            return BehaviorRelay<BottomSheetState>.init(value: BottomSheetState.initialState).asObservable()
        }


        var newState = state
        switch state.lifeCycleState {
        case .initial:

            let visibles = BottomSheetState.Visible(title: true, message: false, indicator: true, buttons: true, cancelButton: false)
            let newState = state.new(with: .waitingForResponse, visibles: visibles)
            self.currentState.accept(newState)
            return self.currentState.asObservable()
        case .waitingForResponse:
            let visibles = BottomSheetState.Visible(title: true, message: true, indicator: true, buttons: true, cancelButton: false)

            let newState = state.newFor(success: !state.isError, with: visibles, loaderHeight: 150)
            self.currentState.accept(newState)
            return self.currentState.asObservable()
            
        case .success:
            let visibles = BottomSheetState.Visible(title: true, message: true, indicator: false, buttons: true, cancelButton: false)

            let newState = state.new(with: .initial, visibles: visibles)
            self.currentState.accept(state)
             return self.currentState.asObservable()

        default:
            break
        }

        return self.currentState.asObservable()
    }

    private lazy var closeAction: Action<Void, Bool> = Action { [weak self] in
        guard let self = self else { return .just(true) }
        self.closeDriver.accept(true)
        return self.closeDriver.asObservable()
    }

    // MARK: Outputs

    public let currentState: BehaviorRelay<BottomSheetState>

    let closeDriver: BehaviorRelay<Bool> = {
        BehaviorRelay<Bool>.init(value: false)
    }()

    // MARK: Stored properties
    public private(set) var disposeBag = DisposeBag()
//    private let router: UnownedRouter<AppRoute>

    // MARK: Initialization

//    init(router: UnownedRouter<AppRoute>, initialState: BottomSheetState) {
//        self.router = router
//        self.state = BehaviorRelay.init(value: initialState)
//    }

    init(initialState: BottomSheetState) {
        
        let relay: BehaviorRelay<BottomSheetState> = BehaviorRelay(value: initialState)
        
        self.currentState = relay
        
        
        
    }
}

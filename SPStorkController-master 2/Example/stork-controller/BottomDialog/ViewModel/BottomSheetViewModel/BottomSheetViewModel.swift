//
//  BottomSheetViewModel.swift
//  stork-controller
//
//  Created by Лысков Павел on 06.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import Action
import RxCocoa
import RxSwift
import XCoordinator

protocol BottomSheetViewModelInput {
    var nextTrigger: AnyObserver<BottomSheetState> { get }
    var closeTrigger: AnyObserver<Void> { get }
    
}

protocol BottomSheetViewModelOutput {
    var currentState: BehaviorRelay<BottomSheetState> { get }
    var closeDriver: BehaviorRelay<Bool> { get }
}

protocol BottomSheetViewModel {
    var input: BottomSheetViewModelInput { get }
    var output: BottomSheetViewModelOutput { get }
}

extension BottomSheetViewModel where Self: BottomSheetViewModelInput & BottomSheetViewModelOutput {
    var input: BottomSheetViewModelInput { return self }
    var output: BottomSheetViewModelOutput { return self }
}

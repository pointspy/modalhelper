//
//  Token.swift
//  stork-controller
//
//  Created by Pavel Lyskov on 25.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

struct Token {
    let current: String
    let refresh: String
}

typealias TokenCompletion = (Observable<Token?>) -> Void

struct NetworkLayer {
    static func loginRequest(login: String, password: String) -> Observable<Token?> {
        var token: Token?
        if login == "user", password == "1234" {
            token = Token(current: "8420y20hfu2bcuo2bcoojdncns", refresh: "8420y20hfu2bcuo2bcoojdncns")
        }

        return Observable.just(token).delaySubscription(.seconds(5), scheduler: MainScheduler.instance)
    }
}

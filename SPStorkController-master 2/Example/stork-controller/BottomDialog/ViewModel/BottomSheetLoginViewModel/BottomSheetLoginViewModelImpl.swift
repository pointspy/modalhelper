//
//  BottomSheetViewModelImpl.swift
//  stork-controller
//
//  Created by Лысков Павел on 06.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import Action
import RxCocoa
import RxSwift
import XCoordinator
// swiftlint:disable all
class BottomSheetLoginViewModelImpl: BottomSheetLoginViewModel, BottomSheetLoginViewModelInput, BottomSheetLoginViewModelOutput {
    // MARK: Inputs
    
    private(set) lazy var nextTrigger = nextAction.inputs
    private(set) lazy var closeTrigger = closeAction.inputs
    
    private var login: String = ""
    
    var inputStateRelay: BehaviorRelay<BottomSheetLoginState> = .init(value: .initialState)
    
    // MARK: Actions
    
    var oldState: BottomSheetLoginState?
    
    private lazy var nextAction: Action<(String, String), Token?> = Action { [weak self] credentials in
        guard let self = self else { return Observable.just(nil) }
        let (login, password) = credentials
        
        if self.inputStateRelay.value.lifeCycleState == .success {
            self.closeDriver.accept(true)
            return BehaviorRelay<Token?>.init(value: self.tokenRelay.value).asObservable()
        }
        
        let visibles = BottomSheetLoginState.Visible(title: true, message: false, indicator: true, buttons: true, textFields: false)
        let newState = self.inputStateRelay.value.new(with: BottomSheetLoginState.LifeCycleState.waitingForResponse, visibles: visibles)
        
        self.inputStateRelay.accept(newState)
        self.login = login
        return NetworkLayer.loginRequest(login: login, password: password)
    }
    
    private lazy var closeAction: Action<Void, Bool> = Action { [weak self] in
        guard let self = self else { return .just(true) }
        self.closeDriver.accept(true)
        return self.closeDriver.asObservable()
    }
    
    // MARK: Outputs
    
    public var tokenRelay: BehaviorRelay<Token?> = .init(value: nil)
    
    public var currentState: Driver<BottomSheetLoginState>
    
    let closeDriver: BehaviorRelay<Bool> = {
        BehaviorRelay<Bool>.init(value: false)
    }()
    
    // MARK: Stored properties
    
    public private(set) var disposeBag = DisposeBag()
    
//    private let router: UnownedRouter<AppRoute>
    
    // MARK: Initialization
    
    init(initialState: BottomSheetLoginState) {
        func transformState(for relay: BehaviorRelay<BottomSheetLoginState>) -> Driver<BottomSheetLoginState> {
            return relay.asDriver().flatMapLatest { state -> Driver<BottomSheetLoginState> in
                
                BehaviorRelay<BottomSheetLoginState>.init(value: state).asDriver()
            }
        }
        
        self.currentState = transformState(for: inputStateRelay)
        
        nextAction.elements
            .observeOn(MainScheduler.instance)
            .share(replay: 1, scope: .whileConnected)
            .asDriver(onErrorJustReturn: nil)
            .asObservable()
            .bind(to: tokenRelay)
            .disposed(by: disposeBag)
        
        tokenRelay.asObservable().subscribe(onNext: { [weak self] token in
            
            guard let self = self else { return }
            
            guard token != nil else {
                if let state = self.oldState {
                    let visibles = BottomSheetLoginState.Visible(title: true, message: true, indicator: false, buttons: true, textFields: true)
                    var newState = state.newFor(success: false, with: visibles, loaderHeight: 80, newMessage: "Ошибка авторизации. Проверьте введенные данные и повторите попытку.".at
                        .attributed {
                            $0.font(BottomSheetLoginState.messageFont).foreground(color: .systemRed)
                    })
                    newState.setError(with: true)
                    self.inputStateRelay.accept(newState)
                } else {
                    self.oldState = self.inputStateRelay.value
                }
                
                return
            }
            
            let visibles = BottomSheetLoginState.Visible(title: true, message: true, indicator: true, buttons: true, textFields: false)
            var mess = "Привет, ".at.attributed {
                $0.font(BottomSheetLoginState.messageFont).foreground(color: BottomSheetLoginState.Color.message)
            }
            
            mess = mess + "\(self.login).".at.attributed {
                $0.font(.boldSystemFont(ofSize: 18)).foreground(color: BottomSheetLoginState.Color.title)
            } + " Приложение готово к работе.".at.attributed {
                $0.font(BottomSheetLoginState.messageFont).foreground(color: BottomSheetLoginState.Color.message)
            }
            
            var newState = self.inputStateRelay.value.newFor(success: true, with: visibles, loaderHeight: 100, newMessage: mess)
            newState.setError(with: false)
            self.inputStateRelay.accept(newState)
            
        }).disposed(by: disposeBag)
    }
}

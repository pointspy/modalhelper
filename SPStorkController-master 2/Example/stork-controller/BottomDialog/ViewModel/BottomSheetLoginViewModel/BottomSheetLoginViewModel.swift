//
//  BottomSheetViewModel.swift
//  stork-controller
//
//  Created by Лысков Павел on 06.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import Action
import RxCocoa
import RxSwift
import XCoordinator

protocol BottomSheetLoginViewModelInput {
    var nextTrigger: AnyObserver<(String, String)> { get }
    var closeTrigger: AnyObserver<Void> { get }
//    var loginRelay: BehaviorRelay<String> { get set }
//    var passwordRelay: BehaviorRelay<String> { get set }
//    var loginValid: BehaviorRelay<Bool> { get set }
//    var passwordValid: BehaviorRelay<Bool> { get set }
}

protocol BottomSheetLoginViewModelOutput {
    var currentState: Driver<BottomSheetLoginState> { get }
    var tokenRelay: BehaviorRelay<Token?> { get }
    var closeDriver: BehaviorRelay<Bool> { get }
//    var isValidRelay: Driver<Bool> { get }
//    var login: Driver<String> { get }
//    var password: Driver<String> { get }
}

protocol BottomSheetLoginViewModel {
    var input: BottomSheetLoginViewModelInput { get }
    var output: BottomSheetLoginViewModelOutput { get }
}

extension BottomSheetLoginViewModel where Self: BottomSheetLoginViewModelInput & BottomSheetLoginViewModelOutput {
    var input: BottomSheetLoginViewModelInput { return self }
    var output: BottomSheetLoginViewModelOutput { return self }
}

//
//  UIFont+Ext.swift
//  stork-controller
//
//  Created by Лысков Павел on 03.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import UIKit

public extension UIFont {
    
    func sizetFor(text: String) -> CGSize {
        let attributes = [NSAttributedString.Key.font: self]
        let size = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        let options: NSStringDrawingOptions = [.usesFontLeading, .usesLineFragmentOrigin]
        let bounds = (text as NSString).boundingRect(with: size, options: options, attributes: attributes, context: nil)
        
        return bounds.size
    }
    
    func heightFor(text: String, width: CGFloat) -> CGFloat {
        let attributes = [NSAttributedString.Key.font: self]
        let size = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let options: NSStringDrawingOptions = [.usesFontLeading, .usesLineFragmentOrigin]
        let bounds = (text as NSString).boundingRect(with: size, options: options, attributes: attributes, context: nil)
        
        return bounds.size.height
    }
    
}

//
//  BorderType.swift
//  Acceptio
//
//  Created by Pavel Lyskov on 04/08/2019.
//  Copyright © 2019 Pavel Lyskov. All rights reserved.
//

import Foundation

public enum BorderType: IBEnum {
    case solid
    case dash(dashLength: Int, spaceLength: Int)
    case none
}

extension BorderType {
    
    public init(string: String?) {
        guard let string = string else {
            self = .none
            return
        }

        switch string {
        case "solid":
            self = .solid
        case "dash":
            self = .dash(dashLength: 1, spaceLength: 1)
        default:
            self = .none
        }
    }
}

extension BorderType: Equatable {
}

public func == (lhs: BorderType, rhs: BorderType) -> Bool {
    switch (lhs, rhs) {
    case (.solid, .solid):
        return true
    case (.dash, .dash):
        return true
    default:
        return false
    }
}

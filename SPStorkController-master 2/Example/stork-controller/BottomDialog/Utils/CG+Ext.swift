//
//  CG+Ext.swift
//  stork-controller
//
//  Created by Лысков Павел on 18.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import UIKit

extension CGRect {
    var zeroHeightFrame: CGRect {
        return CGRect(x: minX, y: midY, width: width, height: 1.0)
    }

    var translatedFrameForY: CGRect {
        return CGRect(x: minX, y: minY + 30.0, width: width, height: height)
    }
}

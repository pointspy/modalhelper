//
//  Node+Ext.swift
//  stork-controller
//
//  Created by Pavel Lyskov on 19.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import AsyncDisplayKit

public extension ASButtonNode {
    
    func set(enabled: Bool) {
        
        self.isEnabled = enabled
        self.alpha = enabled ? 1.0 : 0.6
        
    }
    
}

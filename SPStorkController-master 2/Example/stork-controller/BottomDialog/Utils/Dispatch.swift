//
//  Dispatch.swift
//  stork-controller
//
//  Created by Лысков Павел on 11.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//
import UIKit

public class Dispatch {
    open class func delay(_ delay: Double, closure: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime
                .now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC),
            execute: closure
        )
    }

    open class func background(_ block: @escaping () -> Void) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            block()
        }
    }

    open class func foreground(_ block: @escaping () -> Void) {
        DispatchQueue.main.async { () -> Void in
            block()
        }
    }
}

func delay(_ delay: Double, closure: @escaping () -> Void) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when) {
        closure()
    }
}

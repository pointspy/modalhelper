//
//  Array+Ext.swift
//  stork-controller
//
//  Created by Лысков Павел on 11.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import UIKit

extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}


//
//  ConfigureUtil.swift
//  stork-controller
//
//  Created by Pavel Lyskov on 18.03.2020.
//  Copyright © 2020 Ivan Vorobei. All rights reserved.
//

import UIKit
import AsyncDisplayKit

func configure<T>(
    _ value: T,
    using closure: (inout T) throws -> Void
) rethrows -> T {
    var value = value
    try closure(&value)
    return value
}

extension UIView {
    func layout(using constraints: [NSLayoutConstraint]) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(constraints)
    }
}



extension ASDisplayNode {
    
    func setRegularCorner(radius: CGFloat) {
        
        self.cornerRadius = radius
        self.cornerRoundingType = .defaultSlowCALayer
        
    }
    
}

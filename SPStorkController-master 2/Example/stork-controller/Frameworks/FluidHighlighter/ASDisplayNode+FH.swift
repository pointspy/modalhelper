import AsyncDisplayKit

public extension FluidHighlighter where Base: ASDisplayNode {
    // MARK: - Public methods
    
    func enable(normalColor: UIColor,
                highlightedColor: UIColor,
                highlightedOptions: UIView.AnimationOptions? = nil,
                highlightedDelay: TimeInterval = 0.0,
                highlightedDuration: TimeInterval = 0.217) {
        if base.isNodeLoaded {
            base.view.fh.enable(normalColor: normalColor,
                                highlightedColor: highlightedColor,
                                highlightedOptions: highlightedOptions,
                                highlightedDelay: highlightedDelay,
                                highlightedDuration: highlightedDuration)
        } else {
            base.onDidLoad { node in
                node.view.fh.enable(normalColor: normalColor,
                                    highlightedColor: highlightedColor,
                                    highlightedOptions: highlightedOptions,
                                    highlightedDelay: highlightedDelay,
                                    highlightedDuration: highlightedDuration)
            }
        }
    }
    
    func disable() {
        if base.isNodeLoaded {
            base.view.fh.disable()
        } else {
            base.onDidLoad { node in
                node.view.fh.disable()
            }
        }
    }
}

// ButtonNode
//
//extension FluidHighlighter where Base: ASButtonNode {
//    // MARK: - Public methods
//    
//    func controlEnable(normalColor: UIColor,
//                       highlightedColor: UIColor,
//                       selectedColor: UIColor? = nil,
//                       highlightedOptions: UIView.AnimationOptions? = nil,
//                       highlightedDelay: TimeInterval = 0.0,
//                       highlightedDuration: TimeInterval = 0.5) {
//        if base.isNodeLoaded {
//            base.view.fh.enable(normalColor: normalColor,
//            highlightedColor: highlightedColor,
//            highlightedOptions: highlightedOptions,
//            highlightedDelay: highlightedDelay,
//            highlightedDuration: highlightedDuration)
//        } else {
//            base.onDidLoad { node in
//                node.view.fh.enable(normalColor: normalColor,
//                highlightedColor: highlightedColor,
//                highlightedOptions: highlightedOptions,
//                highlightedDelay: highlightedDelay,
//                highlightedDuration: highlightedDuration)
//            }
//        }
//    }
//    
//    func controlDisable() {
//        if base.isNodeLoaded {
//            base.fh.controlDisable()
//        }
//    }
//}
